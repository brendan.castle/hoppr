---
name: Bug Report
about: Create a report to help us improve
---

<!--
Thank you for reporting a possible bug in Hoppr.
Please fill in as much of the template below as you can.
Version: output of `hoppr version`
Platform: output of `uname -a` (UNIX), or version and 32 or 64-bit (Windows)
-->

- **Version**:
- **Platform**:

<!-- Please provide more details below this comment. -->

/label ~"bug"
