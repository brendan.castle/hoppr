"""Hoppr core plugins package."""
from hoppr.plugins.collect.rpm import CollectRpmPlugin

__all__ = [
    "CollectRpmPlugin",
]
