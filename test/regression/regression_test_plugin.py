"""
Module to perform Hoppr regression tests
"""

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

from __future__ import annotations

import difflib
import sys

from pathlib import Path
from subprocess import CalledProcessError, check_call

from typer.testing import CliRunner

import hoppr.utils

from hoppr import __version__
from hoppr.base_plugins.hoppr import HopprPlugin, hoppr_process, hoppr_rerunner
from hoppr.cli import app
from hoppr.models import HopprContext
from hoppr.models.manifest import Manifest
from hoppr.models.sbom import ExternalReference, Sbom
from hoppr.result import Result

runner = CliRunner()


class RegressionTestPlugin(HopprPlugin):
    """
    Plugin to verify expected output during runtime
    """

    def __init__(self, context: HopprContext, config: dict | None = None) -> None:
        super().__init__(context, config)

        self.test_dir = Path(__file__).parent / (config or {}).get("test_dir", "")

    def _compare_external_refs(self, generated: ExternalReference, expected: ExternalReference) -> None:
        """
        Compare generated externalReferences file paths in consolidated SBOM to expected SBOM
        """
        ref_path = generated.url.replace("file://", "")
        expected_path = str(Path(expected.url.replace("file://", "")).resolve())

        if ref_path != expected_path:
            raise ValueError(f"Generated external reference ({ref_path}) does not match expected ({expected_path}).")

    def _resolve_external_ref(self, file_url: str) -> str:
        """
        Resolve relative file paths in expected SBOM externalReferences which are unknown until runtime
        """
        ref_path = file_url.replace("file://", "")
        abs_path = Path(ref_path).resolve()
        return f"file://{str(abs_path)}"

    def get_version(self) -> str:
        return __version__

    @hoppr_process
    @hoppr_rerunner
    def post_stage_process(self) -> Result:
        """
        Perform validation on files in temp directory
        """
        temp_dir = self.context.collect_root_dir / "generic" / "_metadata_"

        consolidated_sbom = Sbom.parse_file(temp_dir / "_consolidated_bom.json")

        expected_bom_file = self.test_dir / "expected-bom.json"
        expected_bom = Sbom.parse_file(expected_bom_file)

        # Resolve externalReferences before comparison
        for external_ref in expected_bom.externalReferences or []:
            external_ref.url = self._resolve_external_ref(file_url=external_ref.url)

        for component in expected_bom.components:
            for external_ref in component.externalReferences or []:
                external_ref.url = self._resolve_external_ref(file_url=external_ref.url)

        # Remove serialNumber fields before comparison
        consolidated_sbom.serialNumber = None  # type: ignore[assignment]
        expected_bom.serialNumber = None  # type: ignore[assignment]

        if consolidated_sbom != expected_bom:
            diff = "\n".join(
                difflib.unified_diff(
                    consolidated_sbom.json(exclude_none=True, exclude_unset=True, indent=2).splitlines(),
                    expected_bom.json(exclude_none=True, exclude_unset=True, indent=2).splitlines(),
                )
            )

            return Result.fail(f"Generated consolidated SBOM does not match expected SBOM.\n\n{diff}")

        link_file = list(Path.cwd().glob("Collect.*.link"))[0]
        link_data = hoppr.utils.load_file(link_file)
        if not isinstance(link_data, dict):
            return Result.fail(message=f"{link_file} data was not loaded as a dictionary.")

        for product in list(link_data.get("products", {}).keys()):
            if not Path(product).exists():
                return Result.fail(message=f"In-toto product '{product}' not found.")

        return Result.success()


if __name__ == "__main__":
    script_dir, test_dir, project_dir, *_ = list(Path(__file__).parents)
    sys.path.append(str(test_dir))

    test_dirs = [test_dir for test_dir in script_dir.glob("[!_]*") if test_dir.is_dir()]

    for test_dir in test_dirs:
        print(f" Running {test_dir.name} regression tests ".center(100, "="))

        # Reset loaded SBOMs and includes before each test
        Sbom.loaded_sboms.clear()
        Manifest.loaded_manifests.clear()

        try:
            print("Generating in-toto keys...")
            check_call(args=["in-toto-keygen", f"{test_dir / 'product_owner_key'}"])
            check_call(args=["in-toto-keygen", f"{test_dir / 'functionary_key'}"])
        except CalledProcessError as ex:
            raise ex

        cmd = [
            "generate",
            "layout",
            "--transfer",
            f"{test_dir / 'transfer.yml'}",
            "--project-owner-key",
            f"{test_dir / 'product_owner_key'}",
            "--functionary-key",
            f"{test_dir / 'functionary_key'}",
        ]

        print("Generating in-toto layout...")
        result = runner.invoke(app, args=cmd, env={"PYTHONPATH": str(project_dir)}, color=True)

        cmd = [
            "bundle",
            "--transfer",
            f"{test_dir / 'transfer.yml'}",
            "--log",
            "hoppr_log.txt",
            "--attest",
            "--functionary-key",
            f"{test_dir / 'functionary_key'}",
            "--verbose",
            f"{test_dir / 'manifest.yml'}",
        ]

        print("Running hopctl bundle...")
        result = runner.invoke(app, args=cmd, env={"PYTHONPATH": str(project_dir)}, color=True)
        print(result.stdout)

        if result.exit_code != 0:
            raise CalledProcessError(returncode=result.exit_code, cmd=cmd)

        print("Verifying in-toto layout...")

        try:
            check_call(
                args=[
                    "in-toto-verify",
                    "--layout",
                    "in-toto.layout",
                    "--layout-keys",
                    f"{test_dir / 'product_owner_key.pub'}",
                ]
            )
        except CalledProcessError as ex:
            raise ex
