"""
Test module for main module
"""

# pylint: disable=redefined-outer-name

from __future__ import annotations

from pathlib import Path
from typing import TYPE_CHECKING, TypeAlias

import pytest

import hoppr.main
import hoppr.processor

from hoppr.main import HopprProcessor  # type: ignore[attr-defined]
from hoppr.result import Result


if TYPE_CHECKING:
    LogCaptureFixture: TypeAlias = pytest.LogCaptureFixture
    MonkeyPatch: TypeAlias = pytest.MonkeyPatch


class MockProcessor:  # pylint: disable=too-few-public-methods
    """
    Mock HopprProcessor
    """

    metadata_files: list[str] = []

    def __init__(self, *_, **__):
        super().__init__()

    def run(self, *_, **__) -> Result:
        """
        Mock run method
        """
        return Result.fail()


@pytest.mark.parametrize(argnames="expected", argvalues=[Result.fail(), Result.success()])
def test_bundle(expected: Result, monkeypatch: MonkeyPatch, resources_dir: Path, tmp_path: Path):
    """
    Test `hoppr.main.bundle` function
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.main, name="HopprProcessor", value=MockProcessor)
        patch.setattr(target=HopprProcessor, name="run", value=lambda *_, **__: expected)

        if expected.is_fail():
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                hoppr.main.bundle(
                    manifest_file=resources_dir / "manifest" / "manifest.yml",
                    credentials_file=resources_dir / "credential" / "cred-test.yml",
                    transfer_file=resources_dir / "transfer" / "transfer-test.yml",
                    log_file=tmp_path / "hoppr-pytest.log",
                    delivered_sbom=tmp_path / "pytest-sbom.cdx.json",
                )

            assert pytest_wrapped_e.type == SystemExit
            assert pytest_wrapped_e.value.code == 1


@pytest.mark.parametrize(argnames="will_generate_prompt", argvalues=[True, False])
def test_generate_layout_prompt(will_generate_prompt: bool, monkeypatch: MonkeyPatch, resources_dir: Path):
    """
    Test generate_layout fuction with and without prompt
    """
    product_owner_path = Path("product_owner_key")
    functionary_path = Path("functionary_key")

    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.main, name="HopprProcessor", value=MockProcessor)
        patch.setattr(target=HopprProcessor, name="run", value=lambda *_, **__: Result.success())
        patch.setattr(target=hoppr.main, name="prompt", value=lambda *_, **__: "5678")
        patch.setattr(target=hoppr.main, name="generate_in_toto_layout", value=lambda *_, **__: None)

        hoppr.main.generate_layout(
            resources_dir / "transfer" / "transfer-test.yml",
            product_owner_path,
            functionary_path,
            will_generate_prompt,
            "1234",
        )


def test_validate(monkeypatch: MonkeyPatch):
    """
    Test validate method
    """
    # pylint: disable=duplicate-code

    with monkeypatch.context() as patch:
        patch.setenv(name="SOME_USERNAME", value="pipeline-bot")
        patch.setenv(name="SOME_TOKEN", value="SoMe_ToKeN1234!@#$")
        patch.setenv(name="DOCKER_PW", value="DoCKeR_PW1234!@#$")
        patch.setenv(name="DOCKER_ADMIN_PW", value="AdminDockerPW")
        patch.setenv(name="GITLAB_PW", value="GiTLaB_PW1234!@#$")

        hoppr.main.validate(
            [Path("test", "resources", "manifest", "unit", "manifest.yml").resolve()],
            Path("test", "resources", "credential", "cred-test.yml").resolve(),
            Path("test", "resources", "transfer", "transfer-test.yml").resolve(),
        )
