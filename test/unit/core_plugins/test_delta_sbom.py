"""
Test module for DeltaSbom class
"""

# pylint: disable=consider-using-with

from __future__ import annotations

import io
import tarfile

from pathlib import Path
from typing import TypeAlias

import pytest

import hoppr
import hoppr.utils

from hoppr.core_plugins.delta_sbom import DeltaSbom
from hoppr.exceptions import HopprError, HopprLoadDataError
from hoppr.models.manifest import Manifest, Repositories
from hoppr.models.sbom import Component, Sbom

FixtureRequest: TypeAlias = pytest.FixtureRequest
MonkeyPatch: TypeAlias = pytest.MonkeyPatch


def mock_component(purl_str: str | None = None, hashes: list | None = None):
    """
    Return Component object to use in test
    """
    hashes = hashes or []
    for hash_item in hashes:
        hash_item["content"] = hash_item["content"].rjust(32, "0")

    if not purl_str:
        return Component(
            name="test-component",
            type="file",  # type: ignore[arg-type]
            hashes=hashes,
            version="1.0",
            components=[],
        )

    purl = hoppr.utils.get_package_url(purl_str)

    return Component(
        name="test-component",
        purl=purl_str,
        type="file",  # type: ignore[arg-type]
        hashes=hashes,
        version=purl.version,
        components=[],
    )


def test_prestage_success(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch):
    """
    Test DeltaSbom.pre_stage_process method success
    """
    prev_bom = Sbom(specVersion="1.4", version=1, bomFormat="CycloneDX", components=[])
    prev_bom.components.append(mock_component("pkg:docker/path/to/image/image_name@1.2.3"))
    prev_bom.components.append(mock_component("pkg:docker/beta@latest"))

    new_bom = prev_bom.copy(deep=True)
    new_bom.components.append(mock_component("pkg:docker/delta@4.5.6"))

    plugin_fixture.config = {"previous": "previous_source_location"}

    with monkeypatch.context() as patch:
        patch.setattr(target=plugin_fixture.context, name="delivered_sbom", value=new_bom)
        patch.setattr(target=Path, name="exists", value=lambda *_, **__: True)
        patch.setattr(target=plugin_fixture, name="_get_previous_bom", value=lambda *_, **__: prev_bom)

        result = plugin_fixture.pre_stage_process()

        assert result.is_success(), f"Expected SUCCESS result, got {result}"

        # "beta" is in returned bom because "latest" may have changed
        assert len(result.return_obj.components) == 2
        assert result.return_obj.components[0].purl == "pkg:docker/beta@latest"
        assert result.return_obj.components[1].purl == "pkg:docker/delta@4.5.6"


def test_prestage_success_with_override(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch):
    """
    Test DeltaSbom.pre_stage_process method success with override
    """
    plugin_fixture.context.previous_delivery = "cli-override-location"

    prev_bom = Sbom(specVersion="1.4", version=1, bomFormat="CycloneDX", components=[])
    prev_bom.components.append(mock_component("pkg:docker/path/to/image/image_name@1.2.3"))
    prev_bom.components.append(mock_component("pkg:docker/beta@latest"))

    new_bom = prev_bom.copy(deep=True)
    new_bom.components.append(mock_component("pkg:docker/delta@4.5.6"))

    plugin_fixture.config = {"previous": "previous_source_location"}

    with monkeypatch.context() as patch:
        patch.setattr(target=plugin_fixture.context, name="delivered_sbom", value=new_bom)
        patch.setattr(target=Path, name="exists", value=lambda *_, **__: True)
        patch.setattr(target=plugin_fixture, name="_get_previous_bom", value=lambda *_, **__: prev_bom)

        result = plugin_fixture.pre_stage_process()

        assert result.is_success(), f"Expected SUCCESS result, got {result}"

        # "beta" is in returned bom because "latest" may have changed
        assert len(result.return_obj.components) == 2
        assert result.return_obj.components[0].purl == "pkg:docker/beta@latest"
        assert result.return_obj.components[1].purl == "pkg:docker/delta@4.5.6"


@pytest.mark.parametrize(
    argnames="fail_on_empty",
    argvalues=[True, False],
)
def test_prestage_no_change(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch, fail_on_empty: bool):
    """
    Test DeltaSbom.pre_stage_process method fails when new SBOM is unchanged from previous SBOM
    """
    prev_bom = Sbom(specVersion="1.4", version=1, bomFormat="CycloneDX", components=[])
    prev_bom.components.append(mock_component("pkg:docker/path/to/image/image_name@1.2.3"))
    prev_bom.components.append(mock_component("pkg:docker/beta@4.5.6"))

    new_bom = prev_bom.copy(deep=True)

    plugin_fixture.config = {"previous": "previous_source_location", "fail_on_empty": fail_on_empty}

    with monkeypatch.context() as patch:
        patch.setattr(target=plugin_fixture.context, name="delivered_sbom", value=new_bom)
        patch.setattr(target=Path, name="exists", value=lambda *_, **__: True)
        patch.setattr(target=plugin_fixture, name="_get_previous_bom", value=lambda *_, **__: prev_bom)

        result = plugin_fixture.pre_stage_process()

        assert result.message == 'No components updated since "previous_source_location".'

        if fail_on_empty:
            assert result.is_fail(), f"Expected FAIL result, got {result}"
        else:
            assert result.is_success(), f"Expected SUCCESS result, got {result}"


def test_prestage_file_not_found(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch):
    """
    Test DeltaSbom.pre_stage_process method fails when previous SBOM not found
    """
    plugin_fixture.config = {"previous": "previous_source_location"}

    monkeypatch.setattr(target=Path, name="exists", value=lambda *_, **__: False)

    result = plugin_fixture.pre_stage_process()

    assert result.is_fail(), f"Expected FAIL result, got {result}"
    assert result.message == 'Previous source file "previous_source_location" not found.'


def test_prestage_noconfig(plugin_fixture: DeltaSbom):
    """
    Test DeltaSbom.pre_stage_process method success with no config
    """
    result = plugin_fixture.pre_stage_process()

    assert result.is_success(), f"Expected SUCCESS result, got {result}"
    assert result.message == (
        "No previously delivered bundle specified for delta bundle. All components will be delivered"
    )


@pytest.mark.parametrize(
    argnames="source_obj",
    argvalues=[
        pytest.param(Manifest(kind="Manifest", schema_version="v1", repositories=Repositories()), id="manifest"),
        pytest.param(Sbom(), id="sbom"),
    ],
)
def test__get_previous_bom(source_obj: Sbom | Manifest, plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch):
    """
    Test DeltaSbom._get_previous_bom method
    """
    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=lambda *_, **__: source_obj.dict())
    monkeypatch.setattr(target=Manifest, name="load", value=lambda *_, **__: source_obj)
    monkeypatch.setattr(target=Sbom, name="load", value=lambda *_, **__: source_obj)
    monkeypatch.setattr(target=tarfile, name="is_tarfile", value=lambda *_, **__: False)

    bom = plugin_fixture._get_previous_bom("source")  # pylint: disable=protected-access
    assert isinstance(bom, Sbom)


def test__get_previous_bom_load_file_fail(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch):
    """
    Testing a type check that was added only to satisfy mypy
    """
    monkeypatch.setattr(target=tarfile, name="is_tarfile", value=lambda *_, **__: False)
    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=lambda *_, **__: ["sbom", "not", "a", "list"])

    with pytest.raises(expected_exception=HopprLoadDataError, match="Previous delivery file not loaded as dictionary"):
        plugin_fixture._get_previous_bom("source")  # pylint: disable=protected-access


def test__get_previous_bom_tarfile(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch, tmp_path: Path):
    """
    Test DeltaSbom._get_previous_bom method with tarfile
    """

    def _extractfile_patch(*_, **__):
        return io.BytesIO(b'{"bomFormat": "CycloneDX", "specVersion": "1.4", "version": 1, "components": []}')

    monkeypatch.setattr(
        target=tarfile,
        name="open",
        value=lambda *_, **__: tarfile.TarFile(name=tmp_path / "testtarfilename", mode="w"),
    )
    monkeypatch.setattr(target=tarfile.TarFile, name="extractfile", value=_extractfile_patch)
    monkeypatch.setattr(target=tarfile, name="is_tarfile", value=lambda *_, **__: True)

    bom = plugin_fixture._get_previous_bom("source")  # pylint: disable=protected-access
    assert isinstance(bom, Sbom)


def test__get_previous_bom_tarfile_empty_buffer(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch, tmp_path: Path):
    """
    Testing a type check that was added only to satisfy mypy
    """
    monkeypatch.setattr(
        target=tarfile,
        name="open",
        value=lambda *_, **__: tarfile.TarFile(name=tmp_path / "testtarfilename", mode="w"),
    )
    monkeypatch.setattr(target=tarfile.TarFile, name="extractfile", value=lambda *_, **__: None)

    with pytest.raises(HopprError):
        plugin_fixture._get_previous_bom("source")  # pylint: disable=protected-access


def test__get_previous_bom_tarfile_not_dict(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch, tmp_path: Path):
    """
    Testing a type check that was added only to satisfy mypy
    """

    def _extractfile_patch(*_, **__):
        return io.BytesIO(b'["sbom", "not", "a", "list"]')

    monkeypatch.setattr(target=tarfile, name="is_tarfile", value=lambda *_, **__: True)
    monkeypatch.setattr(
        target=tarfile,
        name="open",
        value=lambda *_, **__: tarfile.TarFile(name=tmp_path / "testtarfilename", mode="w"),
    )
    monkeypatch.setattr(target=tarfile.TarFile, name="extractfile", value=_extractfile_patch)

    with pytest.raises(HopprError):
        plugin_fixture._get_previous_bom("source")  # pylint: disable=protected-access


@pytest.mark.parametrize(
    argnames=["new_comp", "prev_comp", "expected"],
    argvalues=[
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            True,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@latest"),
            mock_component("pkg:docker/path/to/image/image_name@latest"),
            False,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@latest", [{"alg": "SHA-1", "content": "1701D"}]),
            mock_component("pkg:docker/path/to/image/image_name@latest", [{"alg": "SHA-1", "content": "1701D"}]),
            True,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            mock_component("pkg:docker/path/to/image/image-name@1.2.3"),
            False,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            mock_component("pkg:generic/path/to/image/image_name@1.2.3"),
            False,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            mock_component("pkg:docker/path/to/image/image_name@1.2.4"),
            False,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            mock_component("pkg:docker/path/from/image/image_name@1.2.3"),
            False,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            mock_component("pkg:docker/path/to/image/image_name@1.2.3#subpath"),
            False,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            mock_component("pkg:docker/path/to/image/image_name@1.2.3?arch=linux"),
            False,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3", [{"alg": "SHA-1", "content": "1701D"}]),
            mock_component("pkg:docker/path/to/image/image_name@1.2.3", [{"alg": "SHA-1", "content": "1701E"}]),
            False,
        ),
        (
            mock_component(
                "pkg:docker/path/to/image/image_name@1.2.3",
                [{"alg": "SHA-1", "content": "1701D"}, {"alg": "SHA-256", "content": "1701D"}],
            ),
            mock_component("pkg:docker/path/to/image/image_name@1.2.3", [{"alg": "SHA-1", "content": "1701D"}]),
            True,
        ),
        (
            mock_component(),
            mock_component("pkg:docker/path/to/image/image_name@1.2.3", [{"alg": "SHA-1", "content": "1701D"}]),
            False,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3", [{"alg": "SHA-1", "content": "1701D"}]),
            mock_component(),
            False,
        ),
    ],
)
def test_comp_match(plugin_fixture: DeltaSbom, new_comp, prev_comp, expected):
    """
    Test DeltaSbom._component_match method
    """
    print(f"New Component:  purl: {new_comp.purl}, hashes: {new_comp.hashes}")
    print(f"Prev Component: purl: {prev_comp.purl}, hashes: {prev_comp.hashes}")

    match = plugin_fixture._component_match(new_comp, prev_comp)  # pylint: disable=protected-access

    assert match is expected
