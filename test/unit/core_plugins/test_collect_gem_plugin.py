"""
Test module for CollectGemPlugin class
"""

# pylint: disable=redefined-outer-name

from typing import Callable

import pytest

from pytest import MonkeyPatch

from hoppr.core_plugins.collect_gem_plugin import CollectGemPlugin, requests  # type: ignore[attr-defined]
from hoppr.models.manifest import Repository
from hoppr.models.sbom import Component
from hoppr.models.types import PurlType

test_component = Component(
    name="TestComponent",
    purl="pkg:gem/example-package@1.2.3?platform=test",
    type="file",  # type: ignore[arg-type]
)


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Test Component fixture
    """
    return test_component


@pytest.fixture
def plugin_fixture(plugin_fixture: CollectGemPlugin, monkeypatch: MonkeyPatch) -> CollectGemPlugin:
    """
    Override and parametrize plugin_fixture to return CollectGemPlugin
    """
    monkeypatch.setattr(
        target=plugin_fixture,
        name="_get_repos",
        value=lambda comp: ["https://somewhere1.com"],
    )

    plugin_fixture.context.repositories[PurlType.GEM] = [
        Repository.parse_obj({"url": "https://somewhere2.com", "description": ""})
    ]

    return plugin_fixture


@pytest.mark.parametrize(
    argnames="response_fixture",
    argvalues=[
        {
            "status_code": 200,
            "content": b'{"platform" : "test", "gem_uri": "elsewhere.com"}',
        }
    ],
    indirect=True,
)
def test_collect_gem_success(
    plugin_fixture: CollectGemPlugin,
    response_fixture: Callable[..., requests.Response],
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test Gem collector successful run with a given URL
    """
    # pylint: disable=duplicate-code
    with monkeypatch.context() as patch:
        patch.setattr(target=requests, name="get", value=response_fixture)

        collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}."


@pytest.mark.parametrize(
    argnames="response_fixture",
    argvalues=[
        {
            "status_code": 404,
            "content": b'{"platform" : "test", "gem_uri": "elsewhere.com"}',
        }
    ],
    indirect=True,
)
def test_collect_gem_fail_bad_purl(
    plugin_fixture: CollectGemPlugin,
    response_fixture: Callable[..., requests.Response],
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test Gem collector fail run if purl is inaccesible
    """
    # pylint: disable=duplicate-code
    with monkeypatch.context() as patch:
        patch.setattr(target=requests, name="get", value=response_fixture)

        collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail(), f"Expected Fail result, got {collect_result}."


@pytest.mark.parametrize(
    argnames="response_fixture",
    argvalues=[{"status_code": 200, "content": b'{"platform" : "test"}'}],
    indirect=True,
)
def test_collect_gem_fail_no_gem_uri(
    plugin_fixture: CollectGemPlugin,
    response_fixture: Callable[..., requests.Response],
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test Gem collector fail run when no gem URI has been returned
    """
    # pylint: disable=duplicate-code
    with monkeypatch.context() as patch:
        patch.setattr(target=requests, name="get", value=response_fixture)

        collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail(), f"Expected Fail result, got {collect_result}."


@pytest.mark.parametrize(
    argnames="response_generator",
    argvalues=[["response_200", "response_404"]],
    indirect=True,
)
def test_collect_gem_fail_bad_gem_uri(
    plugin_fixture: CollectGemPlugin,
    response_generator: Callable[..., requests.Response],
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test Gem collector fail run with a bad gem URI from the API
    """
    # pylint: disable=duplicate-code

    monkeypatch.setattr(target=requests, name="get", value=response_generator)
    monkeypatch.setattr(
        target=requests.Response, name="json", value=lambda *_, **__: {"gem_uri": "https://gem_uri.io/link"}
    )

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}."
