"""
Test module for CompositeCollector class
"""

# pylint: disable=redefined-outer-name

from typing import Callable

import pytest

from pytest import FixtureRequest, MonkeyPatch

import hoppr.utils

from hoppr import Component
from hoppr.constants import ConfigKeys
from hoppr.core_plugins.collect_dnf_plugin import CollectDnfPlugin
from hoppr.core_plugins.collect_nexus_search import CollectNexusSearch
from hoppr.core_plugins.composite_collector import CompositeCollector
from hoppr.exceptions import HopprPluginError
from hoppr.models import HopprContext
from hoppr.result import Result, ResultStatus


@pytest.fixture(name="component")
def component_fixture(request: FixtureRequest) -> Component | None:
    """
    Test Component fixture
    """
    if (purl_string := getattr(request, "param", "pkg:rpm/test_component@0.1.2")) is None:
        return None

    purl = hoppr.utils.get_package_url(purl_string)
    return Component.parse_obj({"name": purl.name, "version": purl.version, "purl": purl_string, "type": "file"})


@pytest.fixture
def config_fixture(
    config_fixture: dict[ConfigKeys, list[dict[str, str]]]  # pylint: disable=unused-argument
) -> dict[ConfigKeys, list[dict[str, str]]]:
    """
    Test plugin config fixture
    """
    return {
        ConfigKeys.PLUGINS: [
            {"name": "hoppr.core_plugins.collect_dnf_plugin"},
            {"name": "hoppr.core_plugins.collect_nexus_search"},
        ]
    }


def test_composite_pre_no_config(context_fixture: HopprContext):
    """
    Test CompositeCollector plugin creation without config raises HopprPluginError
    """
    with pytest.raises(HopprPluginError):
        CompositeCollector(context=context_fixture, config=None)


def test_composite_pre_no_children(context_fixture: HopprContext):
    """
    Test CompositeCollector plugin creation with no child plugins raises HopprPluginError
    """
    with pytest.raises(HopprPluginError):
        CompositeCollector(context=context_fixture, config={ConfigKeys.PLUGINS: []})


comp = CompositeCollector.process_component
post = CompositeCollector.post_stage_process
pre = CompositeCollector.pre_stage_process

success = ResultStatus.SUCCESS
fail = ResultStatus.FAIL


@pytest.mark.parametrize(
    argnames=[
        "dnf_result",
        "nexus_result",
        "process_method",
        "component",
        "expected_result",
        "expected_dnf_calls",
        "expected_nexus_calls",
    ],
    argvalues=[
        pytest.param(success, success, pre, None, success, 1, 1, id="pre_stage_success"),
        pytest.param(success, fail, pre, None, fail, 1, 1, id="pre_stage_fail"),
        pytest.param(success, fail, comp, "pkg:rpm/pytest-rpm@0.1.2", success, 1, 0, id="process_component_success"),
        pytest.param(fail, fail, comp, "pkg:rpm/pytest-rpm@0.1.2", fail, 1, 1, id="process_component_fail"),
        pytest.param(success, success, post, None, success, 1, 1, id="post_stage_success"),
        pytest.param(success, fail, post, None, fail, 1, 1, id="post_stage_fail"),
    ],
    indirect=["component"],
)
def test_composite_processing_method(  # pylint: disable=too-many-arguments
    plugin_fixture: CompositeCollector,
    component: Component | None,
    dnf_result: ResultStatus,
    nexus_result: ResultStatus,
    process_method: Callable[..., Result],
    expected_result: ResultStatus,
    expected_dnf_calls: int,
    expected_nexus_calls: int,
    monkeypatch: MonkeyPatch,
):
    """Patch one of the processing methods for a collector."""
    dnf_pre_stage_called = 0
    nexus_pre_stage_called = 0

    def _dnf_process_method_patch(*_, **__) -> Result:
        nonlocal dnf_pre_stage_called
        dnf_pre_stage_called += 1
        return Result(status=dnf_result)

    def _nexus_process_method_patch(*_, **__) -> Result:
        nonlocal nexus_pre_stage_called
        nexus_pre_stage_called += 1
        return Result(status=nexus_result)

    monkeypatch.setattr(target=CollectDnfPlugin, name=process_method.__name__, value=_dnf_process_method_patch)
    monkeypatch.setattr(target=CollectNexusSearch, name=process_method.__name__, value=_nexus_process_method_patch)

    # Include `comp` parameter if expected by the method being tested, otherwise omit it
    method_args = filter(None, [plugin_fixture, component])
    composite_result = process_method(*method_args)

    assert composite_result.status == expected_result, f"Expected {expected_result} result, got {composite_result}"
    assert dnf_pre_stage_called == expected_dnf_calls
    assert nexus_pre_stage_called == expected_nexus_calls


def test_get_attestation_prods_config(config_fixture):
    """
    Test getting attestation products from user config
    """
    prods = CompositeCollector.get_attestation_products(config_fixture)

    assert prods == ["rpm/*"]


def test_get_attestation_prods_no_config():
    """
    Test getting attestation products without a config
    """
    with pytest.raises(HopprPluginError):
        CompositeCollector.get_attestation_products()
