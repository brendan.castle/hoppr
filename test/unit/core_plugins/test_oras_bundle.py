"""
Test module for OrasBundlePlugin class
"""
from __future__ import annotations

import contextlib
import os
import shutil

from pathlib import Path
from typing import Callable

import pytest

from hoppr_cyclonedx_models.cyclonedx_1_5 import Scope, Type
from pytest import MonkeyPatch

from hoppr.core_plugins.oras_bundle import OrasBundlePlugin
from hoppr.core_plugins.oras_registry import Registry
from hoppr.exceptions import HopprPluginError
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.sbom import Component
from hoppr.result import ResultStatus


def test_oras_missing_client(plugin_fixture: OrasBundlePlugin):
    """
    Test OrasBundlePlugin.get_oras_client method with empty credentials
    """
    try:
        plugin_fixture.get_oras_client("", "")
    except HopprPluginError:
        assert True


def test_oras_correct_client(plugin_fixture: OrasBundlePlugin):
    """
    Test OrasBundlePlugin.get_oras_client method
    """
    registry_object = plugin_fixture.get_oras_client("username", "password")
    assert registry_object.headers["Authorization"] is not None


def test_get_files_from_root_dir_cdx(plugin_fixture: OrasBundlePlugin):
    """
    Test OrasBundlePlugin.get_files_from_root_dir method
    """
    directory = Path("/tmp")
    file_list = [
        f"{str(directory)}/generic/_metadata_/_consolidated_bom.json",
        f"{str(directory)}/generic/_metadata_/_delivered_bom.json",
    ]
    test_root_path = f"{str(directory)}/generic"
    if os.path.exists(test_root_path):
        shutil.rmtree(test_root_path, ignore_errors=False, onerror=None)
    os.makedirs(f"{str(directory)}/generic/_metadata_/")
    for file in file_list:
        with open(file, "a", encoding="utf-8"):
            print()
    val = plugin_fixture.get_files_from_root_dir(file_list=file_list, root_dir=directory)
    shutil.rmtree(test_root_path, ignore_errors=False, onerror=None)
    assert len(val) == 2


def test_verify_contents(plugin_fixture: OrasBundlePlugin):
    """
    Test OrasBundlePlugin.verify_contents method
    """
    with contextlib.suppress(HopprPluginError):
        plugin_fixture.verify_contents(
            components=[Component(name="artifact", version="1.2.3", type=Type("library"))],
            archives=[{"path": "/test/artifact-1.2.3.jar"}],
        )


def test_verify_contents_bad_result(plugin_fixture: OrasBundlePlugin):
    """
    Test OrasBundlePlugin.verify_contents method with bad result
    """
    components = [Component(name="artifact", version="1.2.3", type=Type("library"))]

    with contextlib.suppress(HopprPluginError):
        plugin_fixture.verify_contents(components=components, archives=[{"path": ""}])

    components.append(Component(name="artifact", version="1.2.3", type=Type("library"), scope=Scope.excluded))

    archives = plugin_fixture.verify_contents(components=components, archives=[{"path": "artifact-1.2.3"}])
    assert len(archives) == 0

    components.append(Component(name="artifact", version="1.2.3", type=Type("library"), scope=Scope.required))
    archives = plugin_fixture.verify_contents(components=components, archives=[{"path": "artifact-1.2.4"}])
    assert len(archives) == 1


def test_get_files_from_root_dir(plugin_fixture: OrasBundlePlugin):
    """
    Test OrasBundlePlugin.get_files_from_root_dir method
    """
    directory = Path("docs")
    file_list = ["docs/CHANGELOG.md"]
    val = len(plugin_fixture.get_files_from_root_dir(file_list=file_list, root_dir=directory))
    assert val > 0


@pytest.mark.usefixtures("config_fixture", "cred_object_fixture")
@pytest.mark.parametrize(
    argnames=["config_fixture", "cred_object_fixture", "expected_message"],
    argvalues=[
        (
            {"oras_artifact_version": "1.2.3", "oras_registry": "registry.gitlab.com"},
            {"url": "registry.gitlab.com"},
            "Unexpected Exception: Failed to collect oras artifact name from config.",
        ),
        (
            {
                "oras_artifact_name": "registry.gitlab.com/hoppr/hoppr/test/oras-bundle",
                "oras_registry": "registry.gitlab.com",
            },
            {"url": "registry.gitlab.com"},
            "Unexpected Exception: Failed to collect oras artifact version from config.",
        ),
        (
            {
                "oras_artifact_name": "registry.gitlab.com/hoppr/hoppr/test/oras-bundle",
                "oras_artifact_version": "1.2.3",
            },
            {"url": "registry.gitlab.com"},
            "Unexpected Exception: Failed to collect oras registry from config.",
        ),
        (
            {
                "oras_artifact_name": "https://registry.gitlab.com/hoppr/hoppr/test/oras-bundle",
                "oras_artifact_version": "1.2.3",
                "oras_registry": "https://registry.gitlab.com",
            },
            {"url": "registry.gitlab.com"},
            "Unexpected Exception: Oras Registry name should just be a hostname and not contain a protocol scheme",
        ),
        (
            None,
            {"url": "registry.gitlab.com"},
            "Oras config not correct",
        ),
        (
            {
                "oras_artifact_name": "registry.gitlab.com/hoppr/hoppr/test/oras-bundle",
                "oras_artifact_version": "1.2.3",
                "oras_registry": "registry.gitlab.com",
            },
            None,
            "Unexpected Exception: Credentials must not be empty for Oras Bundle Plugin",
        ),
        (
            {
                "oras_artifact_name": "registry.gitlab.com:443/hoppr/hoppr/test/oras-bundle",
                "oras_artifact_version": "1.2.3",
                "oras_registry": "registry.gitlab.com:443",
            },
            None,
            "Unexpected Exception: Credentials must not be empty for Oras Bundle Plugin",
        ),
    ],
    indirect=["config_fixture"],
)
def test_post_stage_process_exceptions(
    plugin_fixture: OrasBundlePlugin,
    find_credentials_fixture: Callable[[str], CredentialRequiredService | None],
    expected_message: str,
    monkeypatch: MonkeyPatch,
):
    """
    Test OrasBundlePlugin.post_stage_process method success
    """
    monkeypatch.setattr(target=plugin_fixture, name="verify_contents", value=lambda *_, **__: [True])
    monkeypatch.setattr(target=Registry, name="push", value=lambda *_, **__: [True])
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)

    result = plugin_fixture.post_stage_process()
    assert result.is_fail()
    assert result.message == expected_message


def test_post_stage_process_success(
    plugin_fixture: OrasBundlePlugin,
    find_credentials_fixture: CredentialRequiredService,
    monkeypatch: MonkeyPatch,
):
    """
    Test OrasBundlePlugin.post_stage_process method success
    """
    monkeypatch.setattr(target=plugin_fixture, name="verify_contents", value=lambda *_, **__: [True])
    monkeypatch.setattr(target=Registry, name="push", value=lambda *_, **__: [True])

    plugin_fixture.config = {
        "oras_artifact_name": "registry.gitlab-oras.com/hoppr/hoppr/test/oras-bundle",
        "oras_artifact_version": "1.2.3",
        "oras_registry": "registry.gitlab-oras.com",
    }

    # Verify error on no-credentials
    result = plugin_fixture.post_stage_process()
    assert result.is_fail()
    assert result.message == "Unexpected Exception: Credentials must not be empty for Oras Bundle Plugin"

    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setenv(name="MOCH_PASS_ENV", value="testpassword")

    result = plugin_fixture.post_stage_process()
    assert result.status == ResultStatus.SUCCESS


@pytest.mark.usefixtures("config_fixture")
@pytest.mark.parametrize(
    argnames="config_fixture",
    argvalues=[
        {
            "oras_artifact_name": "registry.gitlab.com/hoppr/hoppr/test/oras-bundle",
            "oras_artifact_version": "1.2.3",
            "oras_registry": "registry.gitlab.com",
        },
        {
            "oras_artifact_name": "registry.gitlab.com:443/hoppr/hoppr/test/oras-bundle",
            "oras_artifact_version": "1.2.3",
            "oras_registry": "registry.gitlab.com:443",
        },
    ],
    indirect=True,
)
def test_post_stage_process_success_with_registry(
    plugin_fixture: OrasBundlePlugin,
    find_credentials_fixture: CredentialRequiredService,
    monkeypatch: MonkeyPatch,
    tmp_path: Path,
):
    """
    Test OrasBundlePlugin.post_stage_process method success with registry
    """
    test_file = tmp_path / "artifact-1.2"
    test_file.touch()

    monkeypatch.setattr(
        target=plugin_fixture,
        name="verify_contents",
        value=lambda components, archives: [
            {"path": test_file, "media_type": "test"},
            {"path": "not-a-file", "media_type": "test"},
        ],
    )

    monkeypatch.setattr(target=Registry, name="upload_blob", value=lambda *_, **__: [True])
    monkeypatch.setattr(target=Registry, name="_check_200_response", value=lambda *_, **__: [True])
    monkeypatch.setattr(target=Registry, name="do_request", value=lambda *_, **__: [True])
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setenv(name="MOCH_PASS_ENV", value="testpassword")

    plugin_fixture.context.collect_root_dir = Path("docs")
    result = plugin_fixture.post_stage_process()
    assert result.status == ResultStatus.SUCCESS
