"""
Test module for CollectMavenPlugin class
"""

# pylint: disable=redefined-outer-name

from __future__ import annotations

import importlib
import os
import sys

from pathlib import Path
from subprocess import CompletedProcess
from typing import Callable

import jmespath
import pytest
import requests

from pytest import FixtureRequest, MonkeyPatch

import hoppr.net
import hoppr.plugin_utils

from hoppr.core_plugins.collect_maven_plugin import CollectMavenPlugin
from hoppr.models import HopprContext
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Repository
from hoppr.models.sbom import Component
from hoppr.models.types import PurlType
from hoppr.result import Result

SETTINGS_XML = """<settings>
    <profiles>
        <profile>
            <id>hoppr-test-profile</id>
            <repositories>
                <repository>
                    <id>hoppr-test-repo-1</id>
                    <name>Hoppr Test 1</name>
                    <url>https://somewhere.com</url>
                </repository>
                <repository>
                    <id>hoppr-test-repo-2</id>
                    <name>Hoppr Test 2</name>
                    <url>https://somewhere.else.com</url>
                </repository>
            </repositories>
        </profile>
    </profiles>
</settings>
"""


@pytest.fixture(name="component")
def component_fixture(request: FixtureRequest):
    """
    Test Component fixture
    """
    purl = getattr(request, "param", "pkg:maven/something/else@1.2.3")
    return Component(name="TestMavenComponent", purl=purl, type="file")  # type: ignore[arg-type]


@pytest.fixture
def context_fixture(context_fixture: HopprContext, monkeypatch: MonkeyPatch) -> HopprContext:
    """
    Test Context fixture
    """
    monkeypatch.setattr(target=Path, name="is_file", value=lambda self: True)
    monkeypatch.setattr(target=Path, name="read_text", value=lambda self, encoding: SETTINGS_XML)

    return context_fixture


@pytest.mark.usefixtures("config_fixture")
@pytest.fixture
def plugin_fixture(
    plugin_fixture: CollectMavenPlugin,
    monkeypatch: MonkeyPatch,
    tmp_path: Path,
) -> CollectMavenPlugin:
    """
    Override and parametrize plugin_fixture to return CollectMavenPlugin
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://somewhere.com"])

    plugin_fixture.context.collect_root_dir = tmp_path
    plugin_fixture.config = {"maven_command": "mvn", "maven_opts": ["-D1", "-D2"]}
    plugin_fixture.context.repositories[PurlType.MAVEN] = [
        Repository.parse_obj({"url": "https://somewhere.com", "description": ""})
    ]

    return plugin_fixture


@pytest.mark.parametrize(
    argnames="completed_process_fixture", argvalues=[{"returncode": 0}, {"returncode": 1}], indirect=True
)
def test_collect_maven(
    # pylint: disable=too-many-arguments
    plugin_fixture: CollectMavenPlugin,
    component: Component,
    completed_process_fixture: CompletedProcess,
    find_credentials_fixture: CredentialRequiredService,
    run_command_fixture: CompletedProcess,
    monkeypatch: MonkeyPatch,
):
    """
    Test CollectMavenPlugin.collect method
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=os, name="rename", value=lambda old, new: None)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    if completed_process_fixture.returncode == 0:
        assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"
    else:
        assert collect_result.is_fail()


@pytest.mark.parametrize(
    argnames=["context_fixture", "response_fixture", "expected_result"],
    argvalues=[
        pytest.param({"strict_repos": False}, {"status_code": 200}, "SUCCESS", id="OK (200)"),
        pytest.param({"strict_repos": False}, {"status_code": 401}, "FAIL", id="Unauthorized (401)"),
        pytest.param({"strict_repos": False}, {"status_code": 403}, "FAIL", id="Forbidden (403)"),
        pytest.param({"strict_repos": False}, {"status_code": 404}, "FAIL", id="Not Found (404)"),
        pytest.param({"strict_repos": False}, {"status_code": 500}, "FAIL", id="Internal Server Error (500)"),
        pytest.param({"strict_repos": False}, {"status_code": 502}, "FAIL", id="Bad Gateway (502)"),
    ],
    indirect=["context_fixture", "response_fixture"],
)
def test_collect_maven_experimental(
    context_fixture: HopprContext,
    component: Component,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: MonkeyPatch,
    expected_result: str,
):
    """
    Test CollectMavenPlugin.collect method (experimental)
    """
    monkeypatch.setattr(target=hoppr.net, name="download_file", value=response_fixture)
    monkeypatch.setenv(name="HOPPR_EXPERIMENTAL", value="1")

    module = importlib.import_module("hoppr.core_plugins.collect_maven_plugin")
    module = importlib.reload(module)

    monkeypatch.setattr(
        target=sys.modules[__name__],
        name="CollectMavenPlugin",
        value=getattr(module, "CollectMavenPlugin"),
    )

    monkeypatch.setattr(target=CollectMavenPlugin, name="_check_artifact_hash", value=lambda *_, **__: Result.success())

    plugin = CollectMavenPlugin(context=context_fixture, config=None)

    collect_result = plugin.process_component(component)

    assert collect_result.status.name == expected_result


@pytest.mark.parametrize(
    argnames=["context_fixture", "response_generator"],
    argvalues=[pytest.param({"strict_repos": False}, ["response_200", "response_404", "response_200", "response_404"])],
    indirect=True,
)
def test_collect_maven_experimental_download_pom_fail(
    context_fixture: HopprContext,
    response_generator: Callable[..., requests.Response],
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test CollectMavenPlugin.collect method (experimental) pom file download failure
    """
    monkeypatch.setattr(target=hoppr.net, name="download_file", value=response_generator)
    monkeypatch.setenv(name="HOPPR_EXPERIMENTAL", value="1")

    module = importlib.import_module("hoppr.core_plugins.collect_maven_plugin")
    module = importlib.reload(module)

    monkeypatch.setattr(
        target=sys.modules[__name__],
        name="CollectMavenPlugin",
        value=getattr(module, "CollectMavenPlugin"),
    )

    monkeypatch.setattr(target=CollectMavenPlugin, name="_check_artifact_hash", value=lambda *_, **__: Result.success())

    plugin = CollectMavenPlugin(context=context_fixture, config=None)

    monkeypatch.setattr(target=plugin, name="_get_repos", value=lambda comp: ["https://somewhere.com"])

    collect_result = plugin.process_component(component)

    assert collect_result.is_fail()
    assert collect_result.message.startswith(f"Failed to download pom for Maven artifact {component.purl}")


@pytest.mark.parametrize(
    argnames=["context_fixture", "response_fixture"],
    argvalues=[({"strict_repos": False}, {"status_code": 200})],
    indirect=True,
)
def test_maven_experimental__check_artifact_hash(
    context_fixture: HopprContext,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: MonkeyPatch,
):
    """
    Test CollectMavenPlugin._check_artifact_hash method (experimental)
    """
    monkeypatch.setattr(target=hoppr.net, name="download_file", value=response_fixture)
    monkeypatch.setattr(target=hoppr.net, name="get_file_hash", value=lambda *_, **__: "")
    monkeypatch.setenv(name="HOPPR_EXPERIMENTAL", value="1")

    module = importlib.import_module("hoppr.core_plugins.collect_maven_plugin")
    module = importlib.reload(module)

    monkeypatch.setattr(
        target=sys.modules[__name__],
        name="CollectMavenPlugin",
        value=getattr(module, "CollectMavenPlugin"),
    )

    plugin = CollectMavenPlugin(context=context_fixture, config=None)

    check_hash_result = plugin._check_artifact_hash(  # pylint: disable=protected-access
        "https://somewhere.com/test-artifact.jar", "test-artifact.jar"
    )

    assert check_hash_result.is_fail()
    assert check_hash_result.message == (
        "HTTP Status Code: 200\nSHA1 hash for test-artifact.jar does not match expected hash."
    )


@pytest.mark.parametrize(argnames="context_fixture", argvalues=[{"strict_repos": False}], indirect=True)
def test_maven_no_strict(context_fixture: HopprContext, monkeypatch: MonkeyPatch):
    """
    Test collect method: --no-strict flag
    """
    monkeypatch.setattr(target=context_fixture, name="strict_repos", value=False)
    monkeypatch.setattr(target=Path, name="is_file", value=lambda self: True)
    monkeypatch.setattr(target=Path, name="read_text", value=lambda self, encoding: SETTINGS_XML)

    plugin = CollectMavenPlugin(context=context_fixture, config=None)
    assert plugin.system_repositories == [
        "https://repo.maven.apache.org/maven2",
        "https://somewhere.com",
        "https://somewhere.else.com",
    ]


@pytest.mark.parametrize(argnames="context_fixture", argvalues=[{"strict_repos": False}], indirect=True)
def test_maven_no_strict_no_settings(context_fixture: HopprContext, monkeypatch: MonkeyPatch):
    """
    Test collect method: --no-strict flag
    """
    monkeypatch.setattr(target=context_fixture, name="strict_repos", value=False)
    monkeypatch.setattr(target=Path, name="is_file", value=lambda self: True)
    monkeypatch.setattr(target=jmespath, name="search", value=lambda *args, **kwargs: None)

    plugin = CollectMavenPlugin(context=context_fixture, config=None)
    assert plugin.system_repositories == [
        "https://repo.maven.apache.org/maven2",
        "https://somewhere.com",
        "https://somewhere.else.com",
    ]
