"""
Test module for CollectYumPlugin class
"""
from __future__ import annotations

import importlib
import logging

from subprocess import CompletedProcess
from typing import Iterator

import pytest

from pytest import MonkeyPatch

import hoppr.plugin_utils

from hoppr.core_plugins.collect_yum_plugin import CollectYumPlugin
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.sbom import Component
from hoppr.result import Result


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Test Component fixture
    """

    return Component(
        name="TestComponent",
        purl="pkg:rpm/pidgin@2.7.9-5.el6.2?arch=x86_64",
        type="file",  # type: ignore[arg-type]
    )


def get_repos(comp: Component) -> list[str]:  # pylint: disable=unused-argument
    """
    Mock _get_repos method
    """
    return ["https://somewhere.com"]


@pytest.mark.parametrize(
    argnames="config_fixture", argvalues=[{"yumdownloader_command": "yumdownloader"}], indirect=True
)
@pytest.mark.parametrize(
    argnames="completed_process_fixture",
    argvalues=[{"returncode": 0, "stdout": b"https://somewhere.com"}],
    indirect=True,
)
def test_collect_yum_success(
    plugin_fixture: CollectYumPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    run_command_fixture: CompletedProcess,
):
    """
    Test a successful run of the Yum Collector
    """
    # pylint: disable=duplicate-code
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    plugin_fixture.get_logger().setLevel(logging.DEBUG)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[{"returncode": 1}], indirect=True)
def test_collect_yum_fail(
    plugin_fixture: CollectYumPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    run_command_fixture: CompletedProcess,
    find_credentials_fixture: CredentialRequiredService,
):
    """
    Test a failing run of the Yum Collector
    """
    # pylint: disable=duplicate-code
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith(
        "Failure after 3 attempts, final message yumdownloader failed to locate package for"
    )


def test_collect_yum_command_not_found(
    plugin_fixture: CollectYumPlugin,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test yum collector collect with missing command
    """

    def _get_missing_command(*args, **kwargs) -> Result:  # pylint: disable=unused-argument
        return Result.fail("[mock] command not found")

    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=["https://somewhere.com"])
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=_get_missing_command)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "[mock] command not found"


@pytest.mark.parametrize(
    argnames="config_fixture", argvalues=[{"yumdownloader_command": "yumdownloader"}], indirect=True
)
@pytest.mark.parametrize(
    argnames="completed_process_fixture",
    argvalues=[{"returncode": 0, "stdout": b"https://elsewhere.com"}],
    indirect=True,
)
def test_collect_yum_bad_repo(
    plugin_fixture: CollectYumPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    run_command_fixture: CompletedProcess,
):
    """
    Test yum collector collect with mismatched repo urls
    """

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.message.startswith("Yum download url does not match requested url")


@pytest.mark.parametrize(
    argnames=["config_fixture", "completed_process_generator"],
    argvalues=[
        (
            {"yumdownloader_command": "yumdownloader"},
            ["return_0", "return_1", "return_0", "return_1", "return_0", "return_1"],
        ),
    ],
    indirect=True,
)
def test_collect_yum_fail_download(
    plugin_fixture: CollectYumPlugin,
    completed_process_generator: Iterator[CompletedProcess],
    monkeypatch: MonkeyPatch,
    component: Component,
):
    """
    Test yum collector collect when file exists but cannot be downloaded
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(
        target=plugin_fixture,
        name="run_command",
        value=lambda *args, **kwargs: next(completed_process_generator),
    )

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.message.startswith("Failure after 3 attempts, final message Failed to download Yum artifact")


def test_yum_deprecation_warning():
    """
    Test yum collector warns with `DeprecationWarning` on import
    """
    with pytest.warns(
        expected_warning=DeprecationWarning,
        match="The YUM collector plugin has been deprecated; use either the DNF or RPM collector instead",
    ):
        module = importlib.import_module(name="hoppr.core_plugins.collect_yum_plugin")
        importlib.reload(module)
