"""
Test module for HopprProcessor and StageProcessor classes
"""

# pylint: disable=protected-access
# pylint: disable=redefined-outer-name

from __future__ import annotations

import importlib
import io
import logging
import os
import sys

from importlib.metadata import EntryPoint, EntryPoints
from io import BytesIO, TextIOWrapper
from pathlib import Path
from types import ModuleType
from typing import Callable, Iterator, Literal

import pytest

from pytest import FixtureRequest, MonkeyPatch
from rich.containers import Renderables

import hoppr.main
import hoppr.processor
import hoppr.utils

from hoppr.base_plugins.hoppr import HopprPlugin, hoppr_process
from hoppr.cli.bundle import HopprBundleLayout, HopprBundleSummary
from hoppr.cli.layout import console
from hoppr.exceptions import HopprPluginError
from hoppr.models import HopprContext
from hoppr.models.manifest import Manifest
from hoppr.models.sbom import Component, Sbom
from hoppr.models.transfer import ComponentCoverage, Plugin, StageName, StageRef
from hoppr.models.types import BomAccess, LocalFile, OciFile, UrlFile
from hoppr.processor import Future  # type: ignore[attr-defined]
from hoppr.processor import ThreadPoolExecutor  # type: ignore[attr-defined]
from hoppr.processor import HopprProcessor, StageProcessor
from hoppr.result import Result, ResultStatus


# Import on Unix systems only
if os.name == "posix":
    import pwd


@pytest.mark.usefixtures("component")
class UnitTestPlugin(HopprPlugin):
    """
    HopprPlugin class for unit testing
    """

    test_return_obj: Component | None = None

    def get_version(self) -> str:
        return "0.1.2"

    @hoppr_process
    def pre_stage_process(self):
        return Result.success(return_obj=self.test_return_obj)

    @hoppr_process
    def process_component(self, comp):
        return Result.skip()

    @hoppr_process
    def post_stage_process(self):
        return Result.retry()


test_component_list: list[Component] = [
    Component.parse_obj(
        {
            "name": "README.md",
            "version": "0.1.2",
            "type": "file",
            "purl": "pkg:git/README.md",
        }
    ),
    Component.parse_obj(
        {
            "name": "CHANGELOG.md",
            "version": "0.1.2",
            "type": "file",
            "purl": "pkg:generic/docs/CHANGELOG.md",
        }
    ),
]

test_bom = Sbom.parse_obj(
    {
        "specVersion": "1.4",
        "version": 1,
        "bomFormat": "CycloneDX",
        "components": test_component_list,
    }
)


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Test Component fixture
    """
    return Component.parse_obj(
        {
            "name": "TestComponent",
            "version": "1.0.0",
            "purl": "pkg:deb/test-artifact@1:1.0.0?arch=all",
            "type": "application",
        }
    )


@pytest.fixture(name="processor")
def hoppr_processor_fixture(
    monkeypatch: MonkeyPatch,
    request: FixtureRequest,
    resources_dir: Path,
    tmp_path: Path,
) -> Iterator[HopprProcessor]:
    """
    Fixture to return HopprProcessor object
    """
    credentials_file = resources_dir / "credential" / "cred-test.yml"
    manifest_file = resources_dir / "manifest" / "unit" / "manifest.yml"
    transfer_file = resources_dir / "transfer" / "transfer-test.yml"

    param_dict = dict(getattr(request, "param", {}))
    previous_delivery: Path | None = param_dict.get("previous_delivery")

    # Environment variables required for credentials file validation
    monkeypatch.setenv(name="SOME_USERNAME", value="test_user")
    monkeypatch.setenv(name="SOME_TOKEN", value="SoMe_ToKeN1234!@#$")
    monkeypatch.setenv(name="DOCKER_PW", value="DoCKeR_PW1234!@#$")
    monkeypatch.setenv(name="DOCKER_ADMIN_PW", value="AdminDockerPW")
    monkeypatch.setenv(name="GITLAB_PW", value="GiTLaB_PW1234!@#$")

    monkeypatch.setattr(
        target=hoppr.processor.hoppr.utils,  # type: ignore[attr-defined]
        name="is_basic_terminal",
        value=lambda: False,
    )

    console.width = 100
    hoppr.processor.layout = HopprBundleLayout()
    hoppr.processor.summary_panel = HopprBundleSummary()

    processor = HopprProcessor(
        transfer_file,
        manifest_file,
        credentials_file,
        log_level=logging.DEBUG,
        log_file=tmp_path / "unit_test_log.txt",
        previous_delivery=previous_delivery,
    )

    yield processor

    processor.live_display.stop()


@pytest.fixture
def import_module_fixture() -> Callable[[str], ModuleType]:
    """
    Fixture to mock importlib.import_module function
    """

    def _import_module(name: str) -> ModuleType:
        if name.startswith(__name__):
            return sys.modules[__name__]

        return sys.modules[name]

    return _import_module


@pytest.fixture
def processor_plugin_fixture(
    request: FixtureRequest, context_fixture: HopprContext, config_fixture: dict[str, str]
) -> UnitTestPlugin:
    """
    Fixture to return a plugin object for testing
    """
    param_dict = dict(getattr(request, "param", {}))

    plugin = UnitTestPlugin(context=context_fixture, config=config_fixture)

    plugin.bom_access = param_dict.get("bom_access", BomAccess.NO_ACCESS)
    plugin.test_return_obj = param_dict.get("return_obj")

    return plugin


@pytest.fixture(name="stage")
def stage_processor_fixture(
    request: FixtureRequest,
    monkeypatch: MonkeyPatch,
    import_module_fixture: Callable[[str], ModuleType],
    processor: HopprProcessor,
    context_fixture: HopprContext,
) -> StageProcessor:
    """
    Fixture to return a StageProcessor for testing
    """
    param_dict = dict(getattr(request, "param", {}))

    component_coverage = param_dict.get("component_coverage")
    plugins = param_dict.get("plugins", [])
    stage_name = param_dict.get("stage_name", "test_stage")

    stage_ref = StageRef(name=StageName(stage_name), plugins=plugins, component_coverage=component_coverage)
    stage = StageProcessor(stage_ref, context_fixture, processor.logger)

    target_dir = context_fixture.collect_root_dir / "generic" / "_metadata_"
    target_dir.mkdir(parents=True, exist_ok=True)
    (target_dir / "_run_data_").touch()

    monkeypatch.setattr(target=importlib, name="import_module", value=import_module_fixture)

    stage.plugin_ref_list = stage._get_stage_plugins()

    return stage


@pytest.mark.parametrize(
    argnames=["bom_access", "return_obj", "method_name", "expected_result"],
    argvalues=[
        (BomAccess.NO_ACCESS, None, "pre_stage_process", "SUCCESS"),
        (BomAccess.NO_ACCESS, None, "process_component", "SKIP"),
        (BomAccess.NO_ACCESS, None, "post_stage_process", "RETRY"),
        (BomAccess.NO_ACCESS, None, "not_a_real_process", "FAIL"),
        (BomAccess.COMPONENT_ACCESS, "String is not good", "pre_stage_process", "FAIL"),
        (BomAccess.FULL_ACCESS, "String is not good", "pre_stage_process", "FAIL"),
        (BomAccess.NO_ACCESS, "String is not good", "pre_stage_process", "FAIL"),
    ],
    ids=[
        "no_access_pre_stage",
        "no_access_process",
        "no_access_post_stage",
        "bad_method_name",
        "component_access_wrong_type",
        "full_access_wrong_type",
        "no_access_wrong_type",
    ],
)
def test__run_plugin(  # pylint: disable=too-many-arguments
    bom_access: BomAccess,
    return_obj: str | None,
    context_fixture: HopprContext,
    method_name: str,
    expected_result: Literal["SUCCESS", "FAIL", "RETRY", "SKIP", "EXCLUDED"],
    monkeypatch: MonkeyPatch,
):
    """
    Test hoppr.processor._run_plugin function
    """
    with monkeypatch.context() as patch:
        hoppr.processor.layout = HopprBundleLayout()
        patch.setattr(target=UnitTestPlugin, name="bom_access", value=bom_access)
        patch.setattr(target=UnitTestPlugin, name="test_return_obj", value=return_obj)
        patch.setattr(
            target=hoppr.utils,
            name="plugin_instance",
            value=lambda *args, **kwargs: UnitTestPlugin(context_fixture),
        )

        result = hoppr.processor._run_plugin(
            plugin_name="test.unit.test_processor",
            context=context_fixture,
            config=None,
            method_name=method_name,
            component=None,
        )

        result_check = {
            "SUCCESS": result.is_success,
            "FAIL": result.is_fail,
            "RETRY": result.is_retry,
            "SKIP": result.is_skip,
            "EXCLUDED": result.is_excluded,
        }[expected_result]

        assert result_check(), f"Expected {expected_result} result, got {result}"

        # Check result message if `test_return_obj` specified as a string
        if result.return_obj is not None:
            assert result.message == (
                f"Plugin UnitTestPlugin has BOM access level {bom_access.name}, but returned an object of type str"
            )


def test_hoppr_processor__collect_consolidated_bom(processor: HopprProcessor, context_fixture: HopprContext):
    """
    Test HopprProcess._collect_consolidated_bom method
    """
    processor.context = context_fixture

    processor._collect_consolidated_bom()
    consolidated_bom = processor.context.collect_root_dir / "generic" / "_metadata_" / "_consolidated_bom.json"
    content = consolidated_bom.read_text(encoding="utf-8")

    assert processor.context.consolidated_sbom.json(exclude_none=True, by_alias=True, indent=2) == content


def test_hoppr_processor__collect_manifest_metadata(
    processor: HopprProcessor,
    manifest_fixture: Manifest,
    context_fixture: HopprContext,
    monkeypatch: MonkeyPatch,
    tmp_path: Path,
):
    """
    Test HopprProcessor._collect_manifest_metadata method
    """
    processor.context = context_fixture

    Manifest.loaded_manifests[LocalFile(local=tmp_path / "manifest.yml")] = manifest_fixture
    Manifest.loaded_manifests[UrlFile(url="https://test.hoppr.com/manifest.yml")] = manifest_fixture
    Sbom.loaded_sboms[LocalFile(local=tmp_path / "sbom.json")] = processor.context.sboms[0]
    Sbom.loaded_sboms[OciFile(oci="oci://test.hoppr.com/sbom.json")] = processor.context.sboms[0]
    Sbom.loaded_sboms[UrlFile(url="https://test.hoppr.com/sbom.json")] = processor.context.sboms[0]

    with monkeypatch.context() as patch:
        patch.setattr(target=manifest_fixture, name="includes", value=list(Manifest.loaded_manifests.keys()))
        patch.setattr(target=manifest_fixture, name="sboms", value=list(Sbom.loaded_sboms.keys()))

        processor._collect_manifest_metadata(manifest=manifest_fixture, target_dir=tmp_path)


@pytest.mark.skipif(os.name == "nt", reason="The `pwd` library is not available for the NT (Windows) platform")
def test_hoppr_processor__collect_metadata_posix(
    processor: HopprProcessor, context_fixture: HopprContext, monkeypatch: MonkeyPatch
):
    """
    Test HopprProcessor._collect_metadata method
    """
    test_user = type("test_user_struct", (object,), {"pw_name": "test_user"})

    monkeypatch.setattr(target=processor, name="_collect_file", value=lambda file_name, target_dir: None)
    monkeypatch.setattr(target=processor, name="_collect_manifest_metadata", value=lambda manifest, target_dir: None)

    if os.name == "posix":
        monkeypatch.setattr(target=pwd, name="getpwuid", value=lambda uid: test_user)

    processor.context = context_fixture
    processor._collect_metadata()


@pytest.mark.skipif(os.name == "posix", reason="The `os.getlogin` method is not available on Linux/Darwin platforms")
def test_hoppr_processor__collect_metadata_nt(
    processor: HopprProcessor, context_fixture: HopprContext, monkeypatch: MonkeyPatch
):
    """
    Test HopprProcessor._collect_metadata method
    """
    monkeypatch.setattr(target=os, name="getlogin", value=lambda: "test_user")
    monkeypatch.setattr(target=processor, name="_collect_file", value=lambda file_name, target_dir: None)
    monkeypatch.setattr(target=processor, name="_collect_manifest_metadata", value=lambda manifest, target_dir: None)

    processor.context = context_fixture

    processor._collect_metadata()


@pytest.mark.parametrize(argnames="basic_term", argvalues=[False, True])
def test_hoppr_processor__summarize_results_success(
    processor: HopprProcessor,
    stage: StageProcessor,
    context_fixture: HopprContext,
    basic_term: bool,
    monkeypatch: MonkeyPatch,
):
    """
    Test HopprProcessor._summarize_results method
    """
    processor.context = context_fixture

    monkeypatch.setattr(
        target=hoppr.processor.hoppr.utils,  # type: ignore[attr-defined]
        name="is_basic_terminal",
        value=lambda: basic_term,
    )

    stage.results["pre_stage_process"] = [
        ("plugin-a", None, Result.success()),
        ("plugin-a", None, Result.fail()),
        ("plugin-a", None, Result.retry()),
    ]

    stage.results["process_component"] = [
        ("plugin-a", "my-component", Result.success()),
        ("plugin-a", "my-component", Result.fail()),
        ("plugin-a", "my-component", Result.retry()),
    ]

    stage_ref = StageRef(name=stage.stage_id, plugins=stage.plugin_ref_list)
    processor.stage_processor_map[stage_ref] = stage
    failure_count = processor._summarize_results()
    assert failure_count == 4


def test_hoppr_processor_get_logger(processor: HopprProcessor):
    """
    Test HopprProcessor.get_logger method
    """
    processor.get_logger(log_name="HopprProcessorLogger")


@pytest.mark.parametrize(argnames="processor", argvalues=[{"previous_delivery": Path("bundle.tar.gz")}], indirect=True)
def test_hoppr_processor_run(processor: HopprProcessor, monkeypatch: MonkeyPatch):
    """
    Test HopprProcessor.run method
    """
    monkeypatch.setattr(target=StageProcessor, name="run", value=Result.success)

    assert processor.transfer.stages[0].plugins[0].name == "hoppr.core_plugins.delta_sbom"

    result = processor.run()
    assert result.is_success()


def test_hoppr_processor_run_fail(processor: HopprProcessor, monkeypatch: MonkeyPatch):
    """
    Test HopprProcessor.run method fails with no SBOMs in manifest
    """
    monkeypatch.setattr(target=processor, name="_summarize_results", value=lambda: 42)
    monkeypatch.setattr(target=StageProcessor, name="run", value=lambda self: Result.success())

    result = processor.run()
    assert result.is_fail(), f"Expected FAIL result, got {result}"

    assert processor.get_logger() is not None


def test_hoppr_processor_run_no_bom_fail(processor: HopprProcessor, monkeypatch: MonkeyPatch):
    """
    Test HopprProcessor.run method fails with no SBOMs in manifest
    """
    processor.manifest.sboms = []

    with monkeypatch.context() as patch:
        patch.setattr(target=processor, name="_collect_file", value=lambda *args, **kwargs: None)
        patch.setattr(target=StageProcessor, name="run", value=Result.success)
        patch.setattr(target=processor.manifest.consolidated_sbom, name="components", value=[])
        patch.setattr(target=processor.manifest.consolidated_sbom, name="externalReferences", value=None)

        result = processor.run()
        assert result.is_fail(), f"Expected FAIL result, got {result}"


def test_hoppr_processor_run_stage_fail(processor: HopprProcessor, monkeypatch: MonkeyPatch):
    """
    Test HopprProcessor.run method with stage run failure
    """
    processor.manifest.consolidated_sbom = test_bom.copy(deep=True)

    monkeypatch.setattr(target=processor, name="_summarize_results", value=lambda: 42)
    monkeypatch.setattr(target=StageProcessor, name="run", value=lambda self: Result.fail())

    result = processor.run()
    assert result.is_fail(), f"Expected FAIL result, got {result}"
    assert result.message == "42 failed during this execution"


def test_hoppr_processor_run_stage_fail_ignore_errors(processor: HopprProcessor, monkeypatch: MonkeyPatch):
    """
    Test HopprProcessor.run method with stage run failure and ignore errors
    """
    processor.ignore_errors = True
    processor.manifest.consolidated_sbom = test_bom.copy(deep=True)

    monkeypatch.setattr(target=processor, name="_summarize_results", value=lambda: 42)
    monkeypatch.setattr(target=StageProcessor, name="run", value=Result.fail)

    result = processor.run()
    assert result.is_fail(), f"Expected FAIL result, got {result}"
    assert result.message == "42 failed during this execution"


def test_stage_processor__check_bom_access_component(stage: StageProcessor, monkeypatch: MonkeyPatch):
    """
    Test StageProcessor._check_bom_access method with COMPONENT access
    """
    stage.context.delivered_sbom.components = test_component_list

    stage.plugin_ref_list = [
        Plugin(name="test.unit.test_processor", config=None),
        Plugin(name="test.unit.test_processor", config=None),
    ]

    with monkeypatch.context() as patch:
        patch.setattr(target=UnitTestPlugin, name="bom_access", value=BomAccess.COMPONENT_ACCESS)
        patch.setattr(target=hoppr.utils, name="plugin_class", value=lambda plugin_name: UnitTestPlugin)

        result = stage._check_bom_access()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == (
            "Stage test_stage has one or more plugins with COMPONENT_ACCESS: "
            "UnitTestPlugin, UnitTestPlugin, and required component coverage for the stage of OPTIONAL.\n    "
            "If any plugins have COMPONENT access, the stage required coverage "
            "must be EXACTLY_ONCE or NO_MORE_THAN_ONCE."
        )


def test_stage_processor__check_bom_access_full(stage: StageProcessor, monkeypatch: MonkeyPatch):
    """
    Test StageProcessor._check_bom_access method with FULL access
    """
    stage.context.delivered_sbom.components = test_component_list

    stage.plugin_ref_list = [
        Plugin(name="test.unit.test_processor", config=None),
        Plugin(name="test.unit.test_processor", config=None),
    ]

    with monkeypatch.context() as patch:
        patch.setattr(target=UnitTestPlugin, name="bom_access", value=BomAccess.FULL_ACCESS)
        patch.setattr(target=hoppr.utils, name="plugin_class", value=lambda plugin_name: UnitTestPlugin)

        result = stage._check_bom_access()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == (
            "Stage test_stage has one or more plugins with FULL_ACCESS: "
            "UnitTestPlugin, UnitTestPlugin, and multiple plugins defined for the stage.\n    "
            "Any plugin with FULL BOM access must be the only plugin in the stage"
        )


@pytest.mark.parametrize(argnames="stage", argvalues=[{"component_coverage": "EXACTLY_ONCE"}], indirect=True)
def test_stage_processor__check_component_coverage(stage: StageProcessor):
    """
    Test StageProcessor._check_component_coverage method
    """
    stage.context.delivered_sbom.components = test_bom.components
    stage.required_coverage = stage._get_required_coverage()

    stage._save_result("process_component", "TestPlugin", Result.success(), stage.context.delivered_sbom.components[0])

    assert stage._check_component_coverage("process_component") == 1
    assert len(stage.results["process_component"]) == 2


def test_stage_processor__get_required_coverage(stage: StageProcessor, monkeypatch: MonkeyPatch):
    """
    Test StageProcessor._get_required_coverage method
    """

    class _OptionalCoveragePlugin(UnitTestPlugin):
        default_component_coverage = ComponentCoverage.OPTIONAL

    class _NoMoreThanOnceCoveragePlugin(UnitTestPlugin):
        default_component_coverage = ComponentCoverage.NO_MORE_THAN_ONCE

    def _mock_plugin_class(plugin_name: str):
        match plugin_name:
            case "OptionalCoveragePlugin":
                return _OptionalCoveragePlugin
            case "NoMoreThanOnceCoveragePlugin":
                return _NoMoreThanOnceCoveragePlugin

    assert stage._get_required_coverage() == ComponentCoverage.OPTIONAL

    stage.plugin_ref_list = [
        Plugin(name="OptionalCoveragePlugin", config=None),
        Plugin(name="NoMoreThanOnceCoveragePlugin", config=None),
    ]

    monkeypatch.setattr(target=hoppr.utils, name="plugin_class", value=_mock_plugin_class)

    with pytest.raises(HopprPluginError):
        stage._get_required_coverage()

    stage.config_component_coverage = ComponentCoverage.AT_LEAST_ONCE
    assert stage._get_required_coverage() == ComponentCoverage.AT_LEAST_ONCE


@pytest.mark.parametrize(
    argnames=["failures", "retries", "expected_result"],
    argvalues=[
        (0, 0, Result.success()),
        (0, 1, Result.fail("1 'process_component' processes returned 'retry'")),
        (2, 0, Result.fail("2 'process_component' processes failed")),
        (4, 8, Result.fail("4 'process_component' processes failed, and 8 returned 'retry'")),
    ],
)
def test_stage_processor__get_stage_result(stage: StageProcessor, failures: int, retries: int, expected_result: Result):
    """
    Test StageProcessor._get_required_coverage method
    """
    result = stage._get_stage_result("process_component", failures=failures, retries=retries)
    assert result.status == expected_result.status
    assert result.message == expected_result.message


def test_stage_processor__run_all_fail(context_fixture: HopprContext, stage: StageProcessor, monkeypatch: MonkeyPatch):
    """
    Test StageProcessor._run_all method returns fail result
    """
    monkeypatch.setattr(target=Future, name="result", value=lambda *args, **kwargs: Result.fail())
    monkeypatch.setattr(target=hoppr.processor.layout, name="update_job", value=lambda name, **__: None)
    monkeypatch.setattr(
        target=hoppr.utils,
        name="plugin_instance",
        value=lambda *args, **kwargs: UnitTestPlugin(context=context_fixture),
    )

    stage.context.delivered_sbom.components = test_component_list

    stage.plugin_ref_list = [
        Plugin(name="test.unit.test_processor", config=None),
        Plugin(name="test.unit.test_processor", config=None),
    ]

    result = stage._run_all("pre_stage_process")
    assert result.is_fail(), f"Expected FAIL result, got {result}"


@pytest.mark.parametrize(
    argnames="stage",
    argvalues=[
        {
            "plugins": [
                Plugin(name="hoppr.core_plugins.collect_git_plugin", config=None),
                Plugin(name="hoppr.core_plugins.collect_pypi_plugin", config=None),
            ]
        }
    ],
    indirect=True,
)
def test_stage_processor__run_all(context_fixture: HopprContext, stage: StageProcessor, monkeypatch: MonkeyPatch):
    """
    Test StageProcessor._run_all method returns success result
    """
    monkeypatch.setattr(target=Future, name="result", value=lambda *args, **kwargs: Result.success())
    monkeypatch.setattr(
        target=hoppr.processor.hoppr.utils,  # type: ignore[attr-defined]
        name="plugin_instance",
        value=lambda *args, **kwargs: UnitTestPlugin(context=context_fixture),
    )

    stage.context.delivered_sbom.components = test_component_list

    result = stage._run_all("process_component")
    assert result.is_success(), f"Expected SUCCESS result, got {result}"


@pytest.mark.parametrize(
    argnames=["result", "expected_message"],
    argvalues=[
        pytest.param(Result.success(), "[bold green]SUCCESS[/]", id="success"),
        pytest.param(Result.excluded(), "[bold yellow]EXCLUDED[/]", id="excluded"),
        pytest.param(Result.fail(), "[bold red]FAIL[/]", id="fail"),
    ],
)
def test_stage_processor__report_result(
    stage: StageProcessor,
    result: Result,
    expected_message: str,
    monkeypatch: MonkeyPatch,
):
    """
    Test StageProcessor._update_bom method
    """
    stage.context.delivered_sbom.components = test_component_list

    comp = Component.parse_obj(
        {
            "name": "transfer-test",
            "version": "0.1.2",
            "type": "file",
            "purl": "pkg:generic/MODIFIED_README.md",
        }
    )

    monkeypatch.setattr(target=hoppr.utils, name="is_basic_terminal", value=lambda: False)

    output = hoppr.processor.layout.output_panel.renderable
    assert isinstance(output, Renderables)

    stage._report_result("test_stage", comp, result)
    lines = list(iter(output))  # type: ignore[type-var]
    source_column, msg_column = lines.pop().columns  # type: ignore[union-attr]
    assert list(source_column.cells)[0].markup == "[bold cyan]test_stage[/bold cyan]"
    assert list(msg_column.cells)[0] == f"{expected_message} for pkg:generic/MODIFIED_README.md"


def test_stage_processor__update_bom(stage: StageProcessor):
    """
    Test StageProcessor._update_bom method
    """
    stage.context.delivered_sbom.components = test_component_list

    new_bom = Sbom.parse_obj({"specVersion": "1.4", "version": 1, "bomFormat": "CycloneDX", "components": []})

    new_comp = Component.parse_obj(
        {
            "name": "transfer-test",
            "version": "0.1.2",
            "type": "file",
            "purl": "pkg:generic/MODIFIED_README.md",
        }
    )

    new_bom.components.append(new_comp)
    stage._update_bom(new_bom, None)

    assert len(stage.context.delivered_sbom.components) == 1
    assert stage.context.delivered_sbom.components[0].purl == "pkg:generic/MODIFIED_README.md"


def test_stage_processor__update_bom_component(stage: StageProcessor):
    """
    Test StageProcessor._update_bom method
    """
    stage.context.delivered_sbom.components = test_component_list

    new_comp = Component.parse_obj(
        {
            "name": "transfer-test",
            "version": "0.1.2",
            "type": "file",
            "purl": "pkg:generic/MODIFIED_README.md",
        }
    )

    stage._update_bom(new_comp, test_component_list[0])

    assert stage.context.delivered_sbom.components[0].purl == "pkg:generic/MODIFIED_README.md"


def _mock_open(*args, **kwargs):
    if kwargs["mode"] == "r":
        return io.open(*args, **kwargs)  # pylint: disable=unspecified-encoding

    buffer = BytesIO(initial_bytes=b"")
    wrapper = TextIOWrapper(buffer=buffer)
    wrapper.write = lambda x: 0
    return wrapper


@pytest.mark.parametrize(
    argnames=["check_bom_access_result", "expected_result"],
    argvalues=[(Result.success, ResultStatus.SUCCESS), (Result.fail, ResultStatus.FAIL)],
    ids=["check_bom_access_success", "check_bom_access_fail"],
)
def test_stage_processor_run(
    check_bom_access_result: Callable[..., Result],
    expected_result: ResultStatus,
    stage: StageProcessor,
    plugin_fixture: UnitTestPlugin,
    monkeypatch: MonkeyPatch,
):
    """
    Test StageProcessor.run method
    """
    stage.context.delivered_sbom.components = test_component_list

    monkeypatch.setattr(target=stage, name="_run_all", value=Result.success)
    monkeypatch.setattr(target=stage, name="_check_bom_access", value=check_bom_access_result)
    monkeypatch.setattr(target=hoppr.utils, name="plugin_instance", value=lambda *args: plugin_fixture)
    monkeypatch.setattr(target=stage.logger, name="info", value=lambda *args, **kwargs: None)
    monkeypatch.setattr(target=Path, name="open", value=_mock_open)

    stage.plugin_ref_list = [Plugin(name="test.unit.test_processor", config=None)]

    result = stage.run()
    assert result.status == expected_result, f"Expected {expected_result} result, got {result}"


def test_stage_processor_run_module_not_found(stage: StageProcessor, monkeypatch: MonkeyPatch):
    """
    Test StageProcessor.run method raises ModuleNotFoundError
    """
    stage.context.delivered_sbom.components = test_component_list

    monkeypatch.setattr(target=stage.logger, name="info", value=lambda *args, **kwargs: None)
    monkeypatch.setattr(target=Path, name="open", value=_mock_open)

    stage.plugin_ref_list = [Plugin(name="test.unit.test_processor.NotFoundPlugin", config=None)]

    result = stage.run()
    assert result.is_fail(), f"Expected FAIL result, got {result}"


def test_stage_processor_run_no_plugins_loaded(stage: StageProcessor, monkeypatch: MonkeyPatch):
    """
    Test StageProcessor.run method raises HopprPluginError
    """
    stage.config_component_coverage = ComponentCoverage.EXACTLY_ONCE
    stage.context.delivered_sbom.components = test_component_list

    monkeypatch.setattr(target=stage.logger, name="info", value=lambda *args, **kwargs: None)
    monkeypatch.setattr(target=Path, name="open", value=_mock_open)

    result = stage.run()
    assert result.is_fail(), f"Expected FAIL result, got {result}"
    assert result.message == "No plugins were loaded, but required coverage is EXACTLY_ONCE"


def test_stage_processor_run_default_plugin(stage: StageProcessor, monkeypatch: MonkeyPatch):
    """
    Test StageProcessor.run with default HopprPlugin method definitions
    """

    class _UnitTestDefaultPlugin(HopprPlugin):
        def get_version(self) -> str:
            return "3.4.5"

    entry_points_result = [
        EntryPoint(
            name="test.unit.test_processor", value="test.unit.test_processor:UnitTestUtilsPlugin", group="hoppr.plugin"
        ),
    ]

    monkeypatch.setattr(target=HopprBundleLayout, name="start_job", value=lambda *_, **__: None)
    monkeypatch.setattr(target=HopprBundleLayout, name="stop_job", value=lambda *_, **__: None)
    monkeypatch.setattr(target=EntryPoint, name="load", value=lambda self: sys.modules[__name__])
    monkeypatch.setattr(target=hoppr.utils, name="entry_points", value=lambda group: EntryPoints(entry_points_result))
    monkeypatch.setattr(target=hoppr.utils, name="plugin_class", value=lambda plugin_name: _UnitTestDefaultPlugin)
    monkeypatch.setattr(target=stage.logger, name="info", value=lambda *_, **__: None)
    monkeypatch.setattr(target=Path, name="open", value=_mock_open)

    stage.context.delivered_sbom.components = test_component_list

    stage.plugin_ref_list = [Plugin(name="test.unit.test_processor", config=None)]

    result = stage.run()
    assert result.is_success(), f"Expected SUCCESS result, got {result}"


@pytest.mark.parametrize(
    argnames="stage",
    argvalues=[
        {"plugins": [Plugin(name="test.unit.test_processor", config=None)]},
    ],
    indirect=True,
)
def test_stage_processor_run_retry(
    stage: StageProcessor,
    import_module_fixture: Callable[[str], ModuleType],
    monkeypatch: MonkeyPatch,
):
    """
    Test StageProcessor.run method returns retry result
    """

    def _submit_future(*_, method_name: str, **__) -> Future[Result]:
        future_proc: Future[Result] = Future()
        match method_name:
            case "pre_stage_process":
                future_proc.set_result(Result.skip())
            case "process_component":
                future_proc.set_result(Result.success())
            case "post_stage_process":
                future_proc.set_result(Result.retry())

        return future_proc

    monkeypatch.setattr(target=hoppr.utils, name="plugin_class", value=lambda plugin_name: UnitTestPlugin)
    monkeypatch.setattr(target=importlib, name="import_module", value=import_module_fixture)
    monkeypatch.setattr(target=hoppr.processor.layout, name="advance_job", value=lambda name: None)
    monkeypatch.setattr(target=hoppr.processor.layout, name="is_job_finished", value=lambda name: True)
    monkeypatch.setattr(target=hoppr.processor.layout, name="start_job", value=lambda name: None)
    monkeypatch.setattr(target=hoppr.processor.layout, name="stop_job", value=lambda name: None)
    monkeypatch.setattr(target=hoppr.processor.layout, name="update_job", value=lambda name, **__: None)
    monkeypatch.setattr(target=Path, name="open", value=_mock_open)
    monkeypatch.setattr(target=stage.logger, name="info", value=lambda *args, **kwargs: None)
    monkeypatch.setattr(target=ThreadPoolExecutor, name="submit", value=_submit_future)

    stage.context.delivered_sbom.components = test_component_list

    result = stage.run()
    assert result.is_fail(), f"Expected FAIL result, got {result}"
    assert result.message == "1 'post_stage_process' processes returned 'retry'"


def test_stage_processor_run_success(processor: HopprProcessor, stage: StageProcessor, monkeypatch: MonkeyPatch):
    """
    Test StageProcessor.run method returns success result
    """
    stage.stage_id = processor.transfer.stages[0].name
    stage.plugin_ref_list = processor.transfer.stages[0].plugins

    monkeypatch.setattr(target=stage, name="_run_all", value=Result.success)
    monkeypatch.setattr(target=stage.logger, name="info", value=lambda *args, **kwargs: None)
    monkeypatch.setattr(target=Path, name="open", value=_mock_open)

    result = stage.run()
    assert result.is_success()
