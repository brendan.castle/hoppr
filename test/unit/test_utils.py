"""
Test module for hoppr.utils functions
"""

# pylint: disable=redefined-outer-name

from __future__ import annotations

import importlib
import inspect
import sys

from importlib.metadata import EntryPoint, EntryPoints
from os import PathLike
from pathlib import Path
from types import ModuleType
from typing import Any, TypeVar

import psutil
import pytest

from packageurl import PackageURL
from pytest import MonkeyPatch

import hoppr.utils

from hoppr.base_plugins.hoppr import HopprPlugin
from hoppr.core_plugins.collect_docker_plugin import CollectDockerPlugin
from hoppr.core_plugins.collect_helm_plugin import CollectHelmPlugin
from hoppr.core_plugins.report_generator import ReportGenerator
from hoppr.exceptions import HopprError, HopprLoadDataError, HopprPluginError
from hoppr.models import HopprContext
from hoppr.utils import get_package_url


test_input_data = {
    "alpha": [1, 2, 3],
    "beta": ["dog", "cat"],
    "gamma": {"x": 42, "y": "why not", "z": ["mixed", 7, "array"]},
}

ExpectedException = TypeVar("ExpectedException", bound=BaseException)


class UnitTestUtilsPlugin(HopprPlugin):
    """
    HopprPlugin subclass for testing
    """

    def get_version(self) -> str:
        return "3.4.5"


def test__class_in_module():
    """
    Test hoppr.utils._class_in_module function
    """
    plugin_module = sys.modules[__name__]

    hoppr.utils._class_in_module(plugin_module)  # pylint: disable=protected-access


@pytest.mark.parametrize(
    argnames=["getmembers_result", "expected_message"],
    argvalues=[
        (
            [("FirstPlugin", sys.modules[__name__]), ("SecondPlugin", sys.modules[__name__])],
            "Multiple candidate classes defined in 'test.unit.test_utils': 'FirstPlugin', 'SecondPlugin'",
        ),
        (
            [],
            "No class definition found in 'test.unit.test_utils'.",
        ),
    ],
    ids=["multiple_classes", "no_classes"],
)
def test__class_in_module_exception(getmembers_result: list[tuple], expected_message: str, monkeypatch: MonkeyPatch):
    """
    Test hoppr.utils._class_in_module function raises HopprPluginError
    """

    def _import_module(name: str) -> ModuleType:
        if name == "test.unit.test_utils":
            return sys.modules[__name__]

        return sys.modules["hoppr.base_plugins.hoppr"]

    monkeypatch.setattr(target=importlib, name="import_module", value=_import_module)
    monkeypatch.setattr(target=inspect, name="getmembers", value=lambda *args, **kwargs: getmembers_result)

    plugin_module = sys.modules[__name__]

    with pytest.raises(expected_exception=HopprPluginError, match=expected_message):
        hoppr.utils._class_in_module(plugin_module)  # pylint: disable=protected-access


@pytest.mark.parametrize(
    argnames=["list_in", "expected_result"],
    argvalues=[
        (["one", "two", "three"], ["one", "two", "three"]),
        (["one", "one", "two", "two", "three", "three"], ["one", "two", "three"]),
        (["one", 2, 2, "three", 4, "five"], ["one", 2, "three", 4, "five"]),
        (None, []),
        ([], []),
    ],
)
def test_dedup_list(list_in: list[Any], expected_result: list[Any]):
    """
    Test hoppr.utils.dedup_list function
    """
    assert hoppr.utils.dedup_list(list_in) == expected_result


def test_load_file(tmp_path: Path):
    """
    Test hoppr.utils.load_file function
    """
    file_path = tmp_path / "test_file.json"
    file_path.touch()
    file_path.write_text(data="{}", encoding="utf-8")

    assert hoppr.utils.load_file(file_path) == {}


@pytest.mark.parametrize(
    argnames=["file_path", "expected_message"],
    argvalues=[
        ("nonexistent.json", "{} is not a file, cannot be opened."),
        ("test_file.json", "File {} is empty."),
    ],
    ids=["no_file", "empty_file"],
)
def test_load_file_exception(file_path: PathLike[str], expected_message: str, tmp_path: Path):
    """
    Test hoppr.utils.load_file function raises HopprLoadDataError
    """
    file_path = tmp_path / file_path
    (tmp_path / "test_file.json").touch()

    with pytest.raises(expected_exception=HopprLoadDataError, match=expected_message.format(file_path)):
        hoppr.utils.load_file(file_path)


@pytest.mark.parametrize(
    argnames=["file_contents", "expected_message"],
    argvalues=[
        ("", "Empty string cannot be parsed."),
        ("{{'invalid'}", "Unable to recognize data as either json or yaml"),
        ("__________", "Expected dictionary or list, but contents were loaded and returned as string"),
    ],
    ids=["empty_string", "invalid_content", "string_parsed"],
)
def test_load_string_exception(file_contents: str, expected_message: str):
    """
    Test hoppr.utils.load_string function raises HopprLoadDataError
    """
    with pytest.raises(expected_exception=HopprLoadDataError, match=expected_message):
        hoppr.utils.load_string(file_contents)


def test_obscure_passwords():
    """
    Test hoppr.utils.obscure_passwords function
    """
    command = ["run", "-pwd", "pa$$word", "multi word argument"]
    obscured = hoppr.utils.obscure_passwords(command, ["pa$$word"])
    assert obscured == 'run -pwd [masked] "multi word argument"'


@pytest.mark.parametrize(
    argnames=["plugin_name", "entry_point_load", "expected_result"],
    argvalues=[
        ("test_utils", sys.modules[__name__], UnitTestUtilsPlugin),
        ("UnitTestUtilsPlugin", UnitTestUtilsPlugin, UnitTestUtilsPlugin),
        ("test.unit.test_utils", sys.modules[__name__], UnitTestUtilsPlugin),
    ],
    ids=["module_name", "class_name", "full_module_name"],
)
def test_plugin_class(
    plugin_name: str,
    entry_point_load: type[HopprPlugin] | ModuleType,
    expected_result: type[HopprPlugin],
    monkeypatch: MonkeyPatch,
):
    """
    Test hoppr.utils.plugin_class function
    """
    monkeypatch.setattr(target=EntryPoint, name="load", value=lambda self: entry_point_load)
    monkeypatch.setattr(
        target=hoppr.utils,
        name="entry_points",
        value=lambda group: EntryPoints(
            [EntryPoint(name="test_utils", value="test.unit.test_utils:UnitTestUtilsPlugin", group="hoppr.plugin")]
        ),
    )

    plugin_cls = hoppr.utils.plugin_class(plugin_name)
    assert plugin_cls == expected_result


@pytest.mark.parametrize(
    argnames=["plugin_name", "entry_points_result", "expected_exception", "expected_message"],
    argvalues=[
        (
            "test.unit.test_main",
            [],
            HopprPluginError,
            "No class definition found in 'test.unit.test_main'.",
        ),
        (
            "nonexistent.module.path",
            [],
            ModuleNotFoundError,
            "Unable to locate plug-in 'nonexistent.module.path': No module named 'nonexistent'",
        ),
        (
            "test.unit.test_utils",
            [
                EntryPoint(name="test_utils", value="test.unit.test_utils:UnitTestUtilsPlugin", group="hoppr.plugin"),
                EntryPoint(name="utils_plugin", value="test.unit.test_utils:UnitTestUtilsPlugin", group="hoppr.plugin"),
            ],
            HopprPluginError,
            "Multiple entry points matched 'test.unit.test_utils': 'test_utils', 'utils_plugin'",
        ),
    ],
    ids=["no_class_definition", "nonexistent_module", "multiple_entry_point_definitions"],
)
def test_plugin_class_exception(
    plugin_name: str,
    entry_points_result: list[EntryPoint],
    expected_exception: type[ExpectedException],
    expected_message: str,
    monkeypatch: MonkeyPatch,
):
    """
    Test hoppr.utils.plugin_class function raises expected exceptions
    """
    monkeypatch.setattr(target=EntryPoint, name="load", value=lambda self: sys.modules[__name__])
    monkeypatch.setattr(target=hoppr.utils, name="entry_points", value=lambda group: EntryPoints(entry_points_result))

    with pytest.raises(expected_exception, match=expected_message):
        hoppr.utils.plugin_class(plugin_name)


@pytest.mark.parametrize(
    argnames=["plugin_name", "expected_type"],
    argvalues=[
        ("collect_docker_plugin", CollectDockerPlugin),
        ("CollectHelmPlugin", CollectHelmPlugin),
        ("hoppr.core_plugins.report_generator", ReportGenerator),
        ("hoppr.core_plugins.report_generator.report_generator", ReportGenerator),
    ],
    ids=["module_name", "class_name", "module_path", "full_module_path"],
)
def test_plugin_instance(context_fixture: HopprContext, plugin_name: str, expected_type: type[HopprPlugin]):
    """
    Test hoppr.utils.plugin_instance function
    """
    plugin = hoppr.utils.plugin_instance(plugin_name, context_fixture)
    assert plugin is not None
    assert isinstance(plugin, expected_type)


@pytest.mark.parametrize(
    argnames=["directories", "files", "expected_result"],
    argvalues=[
        (["not_empty", "empty"], ["not_empty/test_file.txt"], {"empty"}),
        (["not_empty", "empty/empty"], ["not_empty/test_file.txt"], {"empty", "empty/empty"}),
    ],
)
def test_remove_empty(directories: list[str], files: list[str], expected_result: set[PathLike[str]], tmp_path: Path):
    """
    Test hoppr.utils.remove_empty function
    """
    for directory in directories:
        (tmp_path / directory).mkdir(parents=True, exist_ok=True)

    for file in files:
        (tmp_path / file).touch()

    # Resolve expected_result paths
    expected_result = {(tmp_path / rel_path) for rel_path in list(expected_result)}

    assert hoppr.utils.remove_empty(tmp_path) == expected_result


def test_remove_empty_exception(tmp_path: Path):
    """
    Test hoppr.utils.remove_empty function raises FileNotFoundError when root dir doesn't exist
    """
    with pytest.raises(FileNotFoundError):
        hoppr.utils.remove_empty(tmp_path / "nonexistent")


@pytest.mark.parametrize(
    argnames=["purl_string", "pkg_url"],
    argvalues=[
        pytest.param(
            "pkg:pypi/atomicwrites@1.4.1",
            PackageURL(type="pypi", name="atomicwrites", namespace=None, version="1.4.1", subpath=None),
            id="unencoded",
        ),
        pytest.param(
            "pkg:npm/%40types%2Fparse-json@4.0.0",
            PackageURL(type="npm", name="parse-json", namespace="@types", version="4.0.0", subpath=None),
            id="encoded",
        ),
        pytest.param(
            "pkg:npm/%40types/color-name@1.1.1",
            PackageURL(type="npm", name="color-name", namespace="@types", version="1.1.1", subpath=None),
            id="partial",
        ),
        pytest.param(
            "pkg:golang/github.com/prometheus/client_golang@v1.14.0#api",
            PackageURL(
                type="golang", name="client_golang", namespace="github.com/prometheus", version="v1.14.0", subpath="api"
            ),
            id="subpath",
        ),
    ],
    indirect=False,
)
def test_get_package_url(purl_string: str, pkg_url: PackageURL):
    """test get_package_url against various purl forms"""
    purl: PackageURL = get_package_url(purl_string)

    assert pkg_url.type == purl.type
    assert pkg_url.name == purl.name
    assert pkg_url.namespace == purl.namespace
    assert pkg_url.version == purl.version
    assert pkg_url.subpath == purl.subpath


@pytest.mark.parametrize(
    argnames=["path", "expected"],
    argvalues=[
        pytest.param(
            "/vagrant/something/else",
            psutil._common.sdiskpart(  # pylint: disable=protected-access
                device="vagrant",
                mountpoint="/vagrant",
                fstype="vboxsf",
                opts="rw,nodev,relatime,iocharset=utf8,uid=1001,gid=1001",
                maxfile=255,
                maxpath=4096,
            ),
            id="vagrant",
        ),
        pytest.param(
            "/tmp/garbage",
            psutil._common.sdiskpart(  # pylint: disable=protected-access
                device="/dev/mapper/rl_rocky8-root",
                mountpoint="/tmp",
                fstype="xfs",
                opts="rw,seclabel,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota",
                maxfile=255,
                maxpath=4096,
            ),
            id="garbage",
        ),
        pytest.param(
            "/none/of/the/above",
            None,
            id="not_found",
        ),
    ],
)
def test_get_partition(path: PathLike, expected: tuple, monkeypatch: MonkeyPatch):
    """
    Test the get_partition utility
    """

    def mock_ismount(self: Path):
        return str(self) in {"/tmp", "/vagrant", "/"}

    monkeypatch.setattr(target=Path, name="is_mount", value=mock_ismount)
    monkeypatch.setattr(target=Path, name="resolve", value=lambda self: Path(path))
    monkeypatch.setattr(
        name="disk_partitions",
        target=psutil,
        value=lambda all: [
            psutil._common.sdiskpart(  # pylint: disable=protected-access
                mountpoint="/vagrant",
                fstype="vboxsf",
                opts="rw,nodev,relatime,iocharset=utf8,uid=1001,gid=1001",
                maxfile=255,
                maxpath=4096,
                device="vagrant",
            ),
            psutil._common.sdiskpart(  # pylint: disable=protected-access
                device="/dev/mapper/rl_rocky8-root",
                mountpoint="/tmp",
                fstype="xfs",
                opts="rw,seclabel,relatime,attr2,inode64,logbufs=8,logbsize=32k,noquota",
                maxfile=255,
                maxpath=4096,
            ),
        ],
    )

    if expected is None:
        with pytest.raises(expected_exception=HopprError, match=f"Unable to identify partition for path '{path}'"):
            hoppr.utils.get_partition(path)
    else:
        assert expected == hoppr.utils.get_partition(path)
