"""
Test module for base pydantic models
"""
from datetime import datetime

import hoppr_cyclonedx_models.cyclonedx_1_5 as cdx

from hoppr.models.annotations import Annotations, AnnotatorOrganizationRequired


def test_hoppr_annotations_model():
    """
    Test the Hoppr Annotations class
    """

    annotator = AnnotatorOrganizationRequired(
        organization=cdx.OrganizationalEntity(bom_ref="test-annotations", name="test-annotations", url=[], contact=[]),
        individual=cdx.OrganizationalContact(
            bom_ref="test-annotations", name="test-annotations", email="test-annotations@gitlab.com", phone="1234567890"
        ),
        component=None,
        service=None,
    )

    model = Annotations(
        bom_ref="test-annotations",
        subjects=[],
        timestamp=datetime(year=1970, month=1, day=1),
        text="super-duper",
        signature=None,
        annotator=annotator,
    )
    hash(model)
