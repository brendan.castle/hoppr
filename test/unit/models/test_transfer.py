"""
Test module for transfer pydantic models
"""
from __future__ import annotations

from pathlib import Path
from typing import Any

import pytest

from pytest import MonkeyPatch

import hoppr.utils

from hoppr.models.transfer import ComponentCoverage, Stage, StageName, Transfer, TransferFile

transfer_dict: dict[str, Any] = {
    "kind": "Transfer",
    "metadata": None,
    "schemaVersion": "v1",
    "max_processes": 3,
    "stages": {
        "Collect": {
            "component_coverage": None,
            "plugins": [
                {"name": "hoppr.core_plugins.collect_apt_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_cargo_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_docker_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_git_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_golang_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_helm_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_maven_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_nuget_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_pypi_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_yum_plugin", "config": None},
                {"name": "hoppr.core_plugins.collect_raw_plugin", "config": None},
            ],
        },
        "Bundle": {
            "component_coverage": None,
            "plugins": [{"name": "hoppr.core_plugins.bundle_tar", "config": {"tarfile_name": "~/tarfile.tar.gz"}}],
        },
    },
}


@pytest.fixture(name="transfer_file")
def transfer_file_fixture(resources_dir: Path) -> Path:
    """
    Fixture to return test transfer file Path
    """
    return resources_dir / "transfer" / "transfer-test.yml"


@pytest.mark.parametrize(
    argnames=["coverage_type", "count", "expected"],
    argvalues=[
        (ComponentCoverage.OPTIONAL, 0, True),
        (ComponentCoverage.OPTIONAL, 1, True),
        (ComponentCoverage.OPTIONAL, 99, True),
        (ComponentCoverage.EXACTLY_ONCE, 0, False),
        (ComponentCoverage.EXACTLY_ONCE, 1, True),
        (ComponentCoverage.EXACTLY_ONCE, 2, False),
        (ComponentCoverage.AT_LEAST_ONCE, 0, False),
        (ComponentCoverage.AT_LEAST_ONCE, 1, True),
        (ComponentCoverage.AT_LEAST_ONCE, 7, True),
        (ComponentCoverage.NO_MORE_THAN_ONCE, 0, True),
        (ComponentCoverage.NO_MORE_THAN_ONCE, 1, True),
        (ComponentCoverage.NO_MORE_THAN_ONCE, 7, False),
    ],
)
def test_component_coverage_accepts_count(coverage_type: ComponentCoverage, count: int, expected: bool):
    """
    Test ComponentCoverage.accepts_count method
    """
    assert_msg = f"{coverage_type} should {'' if expected else 'not '}accept a count of {count}"
    assert coverage_type.accepts_count(count) == expected, assert_msg


def test_get_stage_by_name(transfer_file: Path):
    """
    Test stage indexed lookup
    """

    transfer = TransferFile.parse_file(transfer_file)
    assert isinstance(transfer.stages[StageName("Collect")], Stage)


def test_load_transfer_file(transfer_file: Path):
    """
    Test Transfer.load method with Path input
    """
    Transfer.load(transfer_file)


def test_load_transfer_dict():
    """
    Test Transfer.load method with dict input
    """
    Transfer.load(transfer_dict)


def test_load_transfer_wrong_type():
    """
    Test Transfer.load method with wrong input type
    """
    with pytest.raises(expected_exception=TypeError):
        Transfer.load(0)


def test_delta_already_present():
    """
    Test that a delta stage is not inserted if one already exists
    """
    transfer_data = """
    schemaVersion: v1
    kind: Transfer

    stages:
        Delta:
            plugins:
            - name: hoppr.core_plugins.delta_sbom
        Collect:
            plugins:
            - name: hoppr.core_plugins.collect_apt_plugin
        Bundle:
            plugins:
            - name: hoppr.core_plugins.bundle_tar

    max_processes: 3
    """

    transfer = Transfer.parse_raw(transfer_data)

    assert len(transfer.stages) == 3
    assert transfer.stages[0].name == "Delta"
    assert transfer.stages[0].plugins[0].name == "hoppr.core_plugins.delta_sbom"


def test_transfer_file_parse_file(monkeypatch: MonkeyPatch, tmp_path: Path):
    """
    Test TransferFile.parse_file method
    """
    monkeypatch.setitem(
        dic=transfer_dict,
        name="stages",
        value={
            "Delta": {
                "plugins": [
                    {"name": "delta_sbom", "config": {"previous": "previous-manifest.yml"}},
                ]
            },
            **transfer_dict["stages"],
        },
    )

    transfer = Transfer.load(transfer_dict)
    (tmp_path / "transfer.yml").write_text(transfer.yaml(exclude_none=True, indent=True))

    transfer_file = TransferFile.parse_file(tmp_path / "transfer.yml")

    assert transfer_file.stages["Delta"].plugins[0].config
    assert transfer_file.stages["Delta"].plugins[0].config["previous"] == str(tmp_path / "previous-manifest.yml")


def test_transfer_file_parse_file_fail(transfer_file: Path, monkeypatch: MonkeyPatch):
    """
    Test TransferFile.parse_file method fail to load file
    """
    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=lambda file: None)

    with pytest.raises(expected_exception=TypeError) as pytest_exception:
        TransferFile.parse_file(transfer_file)

    assert pytest_exception.value.args[0] == "Local file content was not loaded as dictionary"


EXPECTED_YAML = """kind: Transfer
schema_version: v1
max_processes: 3
stages:
  _delta_sbom_:
    plugins:
    - name: hoppr.core_plugins.delta_sbom
  Collect:
    plugins:
    - name: hoppr.core_plugins.collect_apt_plugin
    - name: hoppr.core_plugins.collect_cargo_plugin
    - name: hoppr.core_plugins.collect_docker_plugin
    - name: hoppr.core_plugins.collect_git_plugin
    - name: hoppr.core_plugins.collect_golang_plugin
    - name: hoppr.core_plugins.collect_helm_plugin
    - name: hoppr.core_plugins.collect_maven_plugin
    - name: hoppr.core_plugins.collect_nuget_plugin
    - name: hoppr.core_plugins.collect_pypi_plugin
    - name: hoppr.core_plugins.collect_yum_plugin
    - name: hoppr.core_plugins.collect_raw_plugin
  Bundle:
    plugins:
    - name: hoppr.core_plugins.bundle_tar
      config:
        tarfile_name: ~/tarfile.tar.gz
"""


def test_yaml():
    """
    Test Transfer.yaml method
    """
    transfer = Transfer.load(transfer_dict)
    assert transfer.yaml(exclude_none=True, indent=True) == EXPECTED_YAML
