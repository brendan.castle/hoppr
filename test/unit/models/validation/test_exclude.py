"""Test module for pydantic models for `hopctl validate sbom` configuration file."""

from __future__ import annotations

import json

from pathlib import Path
from typing import TYPE_CHECKING, Any

import jmespath
import pytest

from hoppr.models.validation.exclude import ExcludeConfig, JMESPathOptions


if TYPE_CHECKING:
    from pytest import FixtureRequest


@pytest.fixture(name="exclude_config")
def exclude_config_fixture(request: FixtureRequest) -> ExcludeConfig:
    """Fixture to return an ExcludeConfig instance."""
    return ExcludeConfig.parse_obj(getattr(request, "param", {}))


@pytest.fixture(name="sbom_dict")
def sbom_dict_fixture(resources_dir: Path) -> dict[str, Any]:
    """Fixture to return test SBOM data."""
    sbom_path = resources_dir / "validate" / "sboms" / "validate-test-invalid.cdx.json"
    return json.loads(sbom_path.read_text(encoding="utf-8"))


@pytest.mark.parametrize(
    argnames=["obj", "expected"],
    argvalues=[
        pytest.param(
            {"purl": r"regexp:/pkg:deb\/acme\/.*/i"},
            [r"""regexp("purl", 'regexp:/pkg:deb\/acme\/.*/i')"""],
            id="dict-regex-input",
        ),
        pytest.param(
            {"licenses": [{"license": {"id": "SSPL-1.0"}}]},
            ['"licenses"', "[?", '"license"."id"', "==", "'SSPL-1.0'", "]"],
            id="dict-nested-input",
        ),
        pytest.param(
            [{"licenses": [{"license": {"id": "SSPL-1.0"}}]}],
            ['"licenses"', "[?", '"license"."id"', "==", "'SSPL-1.0'", "]"],
            id="list[dict]-input",
        ),
        pytest.param(
            {"components": "jmespath:[? licenses[? license.id == 'BSD-2-Clause']]"},
            ["[? licenses[? license.id == 'BSD-2-Clause']]"],
            id="jmespath-string-input",
        ),
    ],
)
def test_exclude_config__build_jmespath(obj: object, expected: list[str]):
    """Test ExcludeConfig._build_jmespath method."""
    result = ExcludeConfig._build_jmespath(obj)  # pylint: disable=protected-access
    assert result == expected, f"\n  Expected: {expected},\n  Actual:   {result}"


@pytest.mark.parametrize(
    argnames=["exclude_config", "expected"],
    argvalues=[
        pytest.param(
            {"components": [{"licenses": [{"license": {"id": "SSPL-1.0"}}]}]},
            {"components": ["""jmespath:[?"licenses"[?"license"."id"=='SSPL-1.0']]"""]},
            id="spdx-license-object",
        ),
        pytest.param(
            {"components": ["jmespath:[? regexp(name, '/mongodb.*/i')]"]},
            {"components": ["jmespath:[? regexp(name, '/mongodb.*/i')]"]},
            id="jmespath-string",
        ),
        pytest.param(
            {"components": [{"purl": r"regexp:/pkg:deb\/acme\/.*/i"}]},
            {"components": [r"""jmespath:[?regexp("purl", 'regexp:/pkg:deb\/acme\/.*/i')]"""]},
            id="purl-regex-object",
        ),
    ],
    indirect=["exclude_config"],
)
def test_exclude_config_convert_to_jmespath(exclude_config: ExcludeConfig, expected: dict[str, list[str]]):
    """Test ExcludeConfig.convert_to_jmespath validator method."""
    result = exclude_config.dict(by_alias=True)
    assert result == expected, f"\n  Expected: {expected},\n  Actual:   {result}"


@pytest.mark.parametrize(
    argnames=["expression", "expected_components"],
    argvalues=[
        pytest.param(
            r"""components[? regexp("bom-ref", '/pkg:golang\/github.com\/bitnami\/.*/')]""",
            [25, 26, 27],
            id="no-flags",
        ),
        pytest.param(
            "components[? regexp(name, '/mongodb.*/i')]",
            [0, 1, 320],
            id="ignore-case-flag",
        ),
        pytest.param(
            "components[? regexp(cpe, '/mongodb/g')]",
            [0, 1, 85, 86, 87, 88, 89, 90, 91, 92, 320],
            id="global-flag",
        ),
        pytest.param(
            "components[? licenses[? regexp(license.id, '/BSD-2-.*/')]]",
            [7, 11, 15, 74, 252, 277, 279, 283, 303, 314, 315, 321, 335],
            id="nested-property",
        ),
    ],
)
def test_jmespath_regexp_function(sbom_dict: dict[str, Any], expression: str, expected_components: list[int]):
    """Test custom JMESPath `regexp` function."""
    search_results = jmespath.search(expression=expression, data=sbom_dict, options=JMESPathOptions())

    assert search_results == [sbom_dict["components"][idx] for idx in expected_components]
