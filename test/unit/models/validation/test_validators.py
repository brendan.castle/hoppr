"""Test module for SBOM validation check models."""

from __future__ import annotations

from pathlib import Path

import pytest

from hoppr.cli.validate import _config_file_callback
from hoppr.exceptions import HopprValidationError
from hoppr.models import validation
from hoppr.models.validation.code_climate import IssueList
from hoppr.models.validation.validators import SbomValidator, validate


@pytest.mark.parametrize(
    argnames=["sbom_id"],
    argvalues=[
        pytest.param("components", id="components"),
        pytest.param("invalid", id="invalid"),
        pytest.param("last-renewal", id="last-renewal"),
        pytest.param("licenses-field", id="licenses-field"),
        pytest.param("licenses-types", id="licenses-types"),
        pytest.param("merged", id="merged"),
        pytest.param("metadata-authors", id="metadata-authors"),
        pytest.param("metadata-only", id="metadata-only"),
        pytest.param("metadata-supplier", id="metadata-supplier"),
        pytest.param("metadata-timestamp", id="metadata-timestamp"),
        pytest.param("name-field", id="name-field"),
        pytest.param("name-or-id", id="name-or-id"),
        pytest.param("purchase-order", id="purchase-order"),
        pytest.param("sbom-components", id="sbom-components"),
        pytest.param("sbom-spec-version", id="sbom-spec-version"),
        pytest.param("sbom-unique-id", id="sbom-unique-id"),
        pytest.param("sbom-vulnerabilities-field", id="sbom-vulnerabilities-field"),
        pytest.param("supplier-field", id="supplier-field"),
        pytest.param("unique-id", id="unique-id"),
        pytest.param("valid", id="valid"),
        pytest.param("version-field", id="version-field"),
    ],
)
def test_validate(sbom_id: str, resources_dir: Path):
    """Test the `hoppr.models.validation.validators.validate` function."""
    validate_resources_dir = resources_dir.relative_to(Path.cwd()) / "validate"
    sbom_file = validate_resources_dir / "sboms" / f"validate-test-{sbom_id}.cdx.json"
    expected = IssueList.parse_file(validate_resources_dir / "code_climate" / f"expected-issues-{sbom_id}.json")

    assert validate(sbom_file) == expected


@pytest.mark.parametrize(
    argnames=["sbom_id", "profile_name", "config_name"],
    argvalues=[
        pytest.param("expiration", "licensing", None, id="[licensing]-expiration"),
        pytest.param("invalid", "licensing", None, id="[licensing]-invalid"),
        pytest.param("last-renewal", "licensing", None, id="[licensing]-last-renewal"),
        pytest.param("licenses-field", "licensing", None, id="[licensing]-licenses-field"),
        pytest.param("licenses-types", "licensing", None, id="[licensing]-licenses-types"),
        pytest.param("components", "ntia", None, id="[ntia]-components"),
        pytest.param("merged", "ntia", None, id="[ntia]-merged"),
        pytest.param("unique-id", "ntia", None, id="[ntia]-unique-id"),
        pytest.param("components", "strict", None, id="[strict]-components"),
        pytest.param("merged", "strict", None, id="[strict]-merged"),
        pytest.param("invalid", "licensing", "ntia", id="[licensing,ntia]-invalid"),
        pytest.param("licenses-field", "licensing", "ntia", id="[licensing,ntia]-licenses-field"),
    ],
)
def test_validate_configs(sbom_id: str, profile_name: str, config_name: str | None, resources_dir: Path):
    """Test the `hoppr.models.validation.validators.validate` function."""

    config_path = (
        Path(validation.__file__).parent / "profiles" / f"{config_name}.config.yml"
        if config_name
        else None
    )  # fmt: skip

    validate_resources_dir = resources_dir.relative_to(Path.cwd()) / "validate"
    sbom_file = validate_resources_dir / "sboms" / f"validate-test-{sbom_id}.cdx.json"
    expected = IssueList.parse_file(
        validate_resources_dir
        / "code_climate"
        / f"expected-issues-[{','.join(filter(None, [profile_name, config_name]))}]-{sbom_id}.json"
    )

    class _ContextPatch:  # pylint: disable=too-few-public-methods
        def __init__(self, profile_name: str = "default"):
            self.params = {"profile": profile_name}

    _config_file_callback(_ContextPatch(profile_name), config_path)  # type: ignore[arg-type]

    assert validate(sbom_file) == expected


@pytest.mark.parametrize(
    argnames=["spec_version", "expected_results"],
    argvalues=[
        pytest.param("1.3", "Got specVersion 1.3; should be 1.5"),
        pytest.param("1.2", "Got specVersion 1.2; cannot be validated"),
        pytest.param("1.4", "Got specVersion 1.4; should be 1.5"),
        pytest.param("1.6", "Got an invalid specVersion of 1.6; should be 1.5"),
    ],
)
def test_invalid_spec_version(spec_version: str, expected_results: str):
    """Test for cases where the spec version is invalid."""
    with pytest.raises(
        expected_exception=HopprValidationError,
        match=expected_results,
    ):
        SbomValidator._validate_spec_version(spec_version)  # pylint: disable=protected-access
