"""Test module for JSON mapper."""

from __future__ import annotations

from pathlib import Path
from typing import NamedTuple, cast

import pytest

from pytest import MonkeyPatch

from hoppr.exceptions import HopprLoadDataError
from hoppr.models.base import CycloneDXBaseModel
from hoppr.models.sbom import Sbom
from hoppr.models.validation.code_climate import IssueLocationPositionRange, PositionRange, PositionReference
from hoppr.models.validation.json_mapper import JSONLocationMapper


def new_position_range(source_path: str, start_line: int, start_column: int, end_line: int, end_column: int):
    """Utility function to initialize `IssueLocationPositionRange` objects."""
    return IssueLocationPositionRange(
        path=Path(source_path),
        positions=PositionRange(
            begin=PositionReference(line=start_line, column=start_column),
            end=PositionReference(line=end_line, column=end_column),
        ),
    )


class ExpectedLocationsMapping(NamedTuple):
    """Expected data for tests."""

    value: str | dict | list | CycloneDXBaseModel | object
    locations: list[IssueLocationPositionRange]


@pytest.mark.parametrize(
    argnames=["content", "json_path", "expected_location_mappings"],
    argvalues=[
        pytest.param(
            b"0",
            "NUMBER_TEST.json",
            [
                ExpectedLocationsMapping(0, [new_position_range("NUMBER_TEST.json", 1, 1, 1, 1)]),
                ExpectedLocationsMapping("0", [new_position_range("NUMBER_TEST.json", 1, 1, 1, 1)]),
                ExpectedLocationsMapping("100", []),
            ],
            id="number",
        ),
        pytest.param(
            b"-100.5e-2",
            "COMPLEX_NUMBER_TEST.json",
            [
                ExpectedLocationsMapping("-100.5e-2", [new_position_range("COMPLEX_NUMBER_TEST.json", 1, 1, 1, 9)]),
                ExpectedLocationsMapping("-9.1e-3", []),
            ],
            id="complex-number",
        ),
        pytest.param(
            b'"0"',
            "STRING_TEST.json",
            [ExpectedLocationsMapping('"0"', [new_position_range("STRING_TEST.json", 1, 1, 1, 3)])],
            id="string",
        ),
        pytest.param(
            b"true",
            "LITERAL_NAME_TEST.json",
            [ExpectedLocationsMapping("true", [new_position_range("LITERAL_NAME_TEST.json", 1, 1, 1, 4)])],
            id="literal-name",
        ),
        pytest.param(
            b'{"color": "blue"}',
            "OBJECT_TEST.json",
            [
                ExpectedLocationsMapping(
                    {"color": "blue"},
                    [
                        new_position_range("OBJECT_TEST.json", 1, 1, 1, 17),
                        new_position_range("OBJECT_TEST.json", 1, 2, 1, 16),
                    ],
                )
            ],
            id="object",
        ),
        pytest.param(
            b"[100, 100]",
            "DUPLICATE_VALUE_TEST.json",
            [
                ExpectedLocationsMapping(
                    100,
                    [
                        new_position_range("DUPLICATE_VALUE_TEST.json", 1, 2, 1, 4),
                        new_position_range("DUPLICATE_VALUE_TEST.json", 1, 7, 1, 9),
                    ],
                ),
                ExpectedLocationsMapping(
                    [100, 100],
                    [new_position_range("DUPLICATE_VALUE_TEST.json", 1, 1, 1, 10)],
                ),
            ],
            id="duplicate-values",
        ),
        pytest.param(
            b'{"dog": {"has": "bark"}, "tree": {"has": "bark"}}',
            "DUPLICATE_MEMBERS_TEST.json",
            [
                ExpectedLocationsMapping(
                    {"dog": {"has": "bark"}, "tree": {"has": "bark"}},
                    [new_position_range("DUPLICATE_MEMBERS_TEST.json", 1, 1, 1, 49)],
                ),
                ExpectedLocationsMapping(
                    {"has": "bark"},
                    [
                        new_position_range("DUPLICATE_MEMBERS_TEST.json", 1, 9, 1, 23),
                        new_position_range("DUPLICATE_MEMBERS_TEST.json", 1, 10, 1, 22),
                        new_position_range("DUPLICATE_MEMBERS_TEST.json", 1, 34, 1, 48),
                        new_position_range("DUPLICATE_MEMBERS_TEST.json", 1, 35, 1, 47),
                    ],
                ),
                ExpectedLocationsMapping(
                    '{"has": "bark"}',
                    [
                        new_position_range("DUPLICATE_MEMBERS_TEST.json", 1, 9, 1, 23),
                        new_position_range("DUPLICATE_MEMBERS_TEST.json", 1, 10, 1, 22),
                        new_position_range("DUPLICATE_MEMBERS_TEST.json", 1, 34, 1, 48),
                        new_position_range("DUPLICATE_MEMBERS_TEST.json", 1, 35, 1, 47),
                    ],
                ),
            ],
            id="duplicate-members",
        ),
        pytest.param(
            b"""
{
  "groceries": {
    "food": [
      "apple",
      "banana",
      "crayons",
      "yogurt"
    ],
    "drinks": [
      "apple juice",
      "banana juice",
      "crayon juice",
      "yogurt",
      "maybe even some water"
    ],
    "count": [1, 2, 3]
  },
  "abc": 456,
  "nothing": null
}
""",
            "MULTILINE_TEST.json",
            [
                ExpectedLocationsMapping(2, [new_position_range("MULTILINE_TEST.json", 17, 18, 17, 18)]),
                ExpectedLocationsMapping(
                    '"yogurt"',
                    [
                        new_position_range("MULTILINE_TEST.json", 8, 7, 8, 14),
                        new_position_range("MULTILINE_TEST.json", 14, 7, 14, 14),
                    ],
                ),
                ExpectedLocationsMapping(
                    ["apple juice", "banana juice", "crayon juice", "yogurt", "maybe even some water"],
                    [new_position_range("MULTILINE_TEST.json", 10, 15, 16, 5)],
                ),
            ],
            id="multiline",
        ),
        pytest.param(
            b"""
{
  "$schema": "http://cyclonedx.org/schema/bom-1.5.schema.json",
  "bomFormat": "CycloneDX",
  "specVersion": "1.5",
  "serialNumber": "urn:uuid:9c58bfbf-99f6-4515-8c76-fb675b0e504c",
  "version": 1,
  "components": [],
  "externalReferences": [],
  "vulnerabilities": []
}
""",
            "CDX_TEST.json",
            [
                ExpectedLocationsMapping(
                    Sbom.parse_obj(
                        {
                            "$schema": "http://cyclonedx.org/schema/bom-1.5.schema.json",
                            "bomFormat": "CycloneDX",
                            "specVersion": "1.5",
                            "serialNumber": "urn:uuid:9c58bfbf-99f6-4515-8c76-fb675b0e504c",
                            "version": 1,
                            "components": [],
                            "externalReferences": [],
                            "vulnerabilities": [],
                        }
                    ),
                    [new_position_range("CDX_TEST.json", 2, 1, 11, 1)],
                )
            ],
            id="cyclonedx-base-model",
        ),
    ],
)
def test_json_location_mapper_find(
    content: bytes,
    json_path: str,
    expected_location_mappings: list[ExpectedLocationsMapping],
    monkeypatch: MonkeyPatch,
):
    """Tests normal usage of `JSONLocationMapper.find` to obtain the positions of JSON values within a JSON document."""

    with monkeypatch.context() as patch:
        patch.setattr(target=Path, name="read_bytes", value=lambda _: content)

        location_map = JSONLocationMapper.load_file(Path(json_path))

        for expected_value, expected_locations in expected_location_mappings:
            result = location_map.find(expected_value)  # type: ignore[arg-type]
            assert result == expected_locations


@pytest.mark.parametrize(
    argnames=["content"],
    argvalues=[
        pytest.param(b"{123}", id="invalid-object"),
        pytest.param(b'{"key": "value"', id="unclosed-object"),
        pytest.param(b'"s', id="unclosed-string"),
        pytest.param(b"[unknown]", id="unknown-keyword"),
        pytest.param(b"", id="empty-document"),
    ],
)
def test_json_location_mapper_load_invalid_json(content: bytes, monkeypatch: MonkeyPatch):
    """Tests `JSONLocationMapper` loading invalid JSON documents."""
    with monkeypatch.context() as patch:
        patch.setattr(target=Path, name="read_bytes", value=lambda _: content)

        with pytest.raises(ValueError) as ex:
            JSONLocationMapper.load_file(Path("DUMMY.json"))

            assert str(ex) == "Invalid JSON document provided."


@pytest.mark.parametrize(
    argnames=["content", "find_key"],
    argvalues=[
        pytest.param(b"-123", "123-", id="invalid-number"),
        pytest.param(b'"ABC"', '"abc', id="unclosed-string"),
        pytest.param(b'{"key": "value"}', '{"key": "value"', id="unclosed-object"),
    ],
)
def test_parser_invalid_key(content: bytes, find_key: str, monkeypatch: MonkeyPatch):
    """Tests `JSONLocationMapper.find` with invalid keys."""
    with monkeypatch.context() as patch:
        patch.setattr(target=Path, name="read_bytes", value=lambda _: content)

        location_map = JSONLocationMapper.load_file(Path("DUMMY.json"))

        with pytest.raises(HopprLoadDataError) as ex:
            location_map.find(find_key)

            assert str(ex).startswith("Failed to parse as valid JSON")


@pytest.mark.parametrize(
    argnames=["content", "json_path", "expected_location_mappings"],
    argvalues=[
        pytest.param(
            b"-123",
            "NUMBER_TEST.json",
            [
                ExpectedLocationsMapping("-123", [new_position_range("NUMBER_TEST.json", 1, 1, 1, 4)]),
                ExpectedLocationsMapping("2", []),
            ],
            id="number",
        ),
        pytest.param(
            b'"ABC"',
            "STRING_TEST.json",
            [
                ExpectedLocationsMapping('"ABC"', [new_position_range("STRING_TEST.json", 1, 1, 1, 5)]),
                ExpectedLocationsMapping('""', []),
            ],
            id="string",
        ),
        pytest.param(
            b"""
{
  "key": "value"
}
""",
            "MULTILINE_OBJECT_TEST.json",
            [
                ExpectedLocationsMapping(
                    '{"key": "value"}',
                    [
                        new_position_range("MULTILINE_OBJECT_TEST.json", 2, 1, 4, 1),
                        new_position_range("MULTILINE_OBJECT_TEST.json", 3, 3, 3, 16),
                    ],
                )
            ],
            id="multiline-object",
        ),
    ],
)
def test_indexing(
    content: bytes,
    json_path: str,
    expected_location_mappings: list[ExpectedLocationsMapping],
    monkeypatch: MonkeyPatch,
):
    """Tests indexing on `JSONLocationMapper` objects."""

    with monkeypatch.context() as patch:
        patch.setattr(target=Path, name="read_bytes", value=lambda _: content)

        location_map = JSONLocationMapper.load_file(Path(json_path))

        for expected_value, expected_locations in expected_location_mappings:
            result = location_map[cast(str, expected_value)]
            assert result == expected_locations
