"""Test module for pydantic models for `hopctl validate sbom` Code Climate report."""

from __future__ import annotations

from pathlib import Path

import pytest

from rich.console import Console

from hoppr.models.validation.code_climate import Issue, IssueList, IssueLocationLineRange, IssueSeverity, LineRange


FILE_PATH = Path("test") / "unit" / "models" / "validation" / "test_code_climate.py"


@pytest.fixture(name="issue_list")
def _issue_list_fixture() -> IssueList:
    return IssueList.parse_obj(
        [
            {
                "type": "issue",
                "check_name": "pytest-issue-1",
                "description": "pytest issue #1",
                "location": {
                    "path": FILE_PATH,
                    "lines": {"begin": 123, "end": 456},
                },
                "severity": "major",
            },
            {
                "type": "issue",
                "check_name": "pytest-issue-2",
                "description": "pytest issue #2",
                "location": {
                    "path": FILE_PATH,
                    "lines": {"begin": 457, "end": 890},
                },
                "severity": "major",
            },
            {
                "type": "issue",
                "check_name": "pytest-issue-3",
                "description": "pytest issue #3",
                "location": {
                    "path": FILE_PATH,
                    "lines": {"begin": 999, "end": 999},
                },
                "severity": "major",
            },
        ]
    )


def test_issue__hash__(issue_list: IssueList):
    """Test Issue.__hash__() method."""
    assert hash(issue_list[0]) == 472482115755003786
    assert hash(issue_list[1]) == 358298143776621879
    assert hash(issue_list[2]) == 443736409589607658

    # Test that an existing fingerprint is unmodified
    issue = Issue(
        type="issue",
        check_name="pytest-issue-with-fingerprint",
        description="pytest issue with fingerprint",
        location=IssueLocationLineRange(path=Path(FILE_PATH), lines=LineRange(begin=1, end=1)),
        severity=IssueSeverity.MAJOR,
        fingerprint="b7c8ec0ba036f7fd",
    )

    assert issue.fingerprint == "b7c8ec0ba036f7fd"


def test_issue_list_fingerprint_generation(issue_list: IssueList):
    """Test IssueList generates fingerprints if not provided."""
    assert issue_list[0].fingerprint == "6ef37ccc3cfae24c"
    assert issue_list[1].fingerprint == "d81bac27c7c0dc3c"
    assert issue_list[2].fingerprint == "48b65c15def80ec2"


def test_issue_list__iter__(issue_list: IssueList):
    """Test IssueList.__iter__ method."""
    for issue in issue_list:
        assert issue.type == "issue"
        assert issue.severity == "major"


def test_issue_list__len__(issue_list: IssueList):
    """Test IssueList.__len__ method."""
    assert len(issue_list) == 3


def test_issue_list__repr__(issue_list: IssueList):
    """Test IssueList.__repr__ method."""
    assert repr(issue_list) == (
        "IssueList([Issue(type='issue', check_name='pytest-issue-1', "
        "description='pytest issue #1', content=None, categories=None, "
        "location=IssueLocationLineRange(path='test/unit/models/validation/test_code_climate.py', "
        "lines=LineRange(begin=123, end=456)), other_locations=None, trace=None, remediation_points=None, "
        "severity='major', fingerprint='6ef37ccc3cfae24c'), Issue(type='issue', check_name='pytest-issue-2', "
        "description='pytest issue #2', content=None, categories=None, "
        "location=IssueLocationLineRange(path='test/unit/models/validation/test_code_climate.py', "
        "lines=LineRange(begin=457, end=890)), other_locations=None, trace=None, remediation_points=None, "
        "severity='major', fingerprint='d81bac27c7c0dc3c'), Issue(type='issue', check_name='pytest-issue-3', "
        "description='pytest issue #3', content=None, categories=None, "
        "location=IssueLocationLineRange(path='test/unit/models/validation/test_code_climate.py', "
        "lines=LineRange(begin=999, end=999)), other_locations=None, trace=None, remediation_points=None, "
        "severity='major', fingerprint='48b65c15def80ec2')])"
    )


def test_issue_list__rich_repr__(issue_list: IssueList):
    """Test IssueList.__rich_repr__ method."""
    console = Console(width=100)

    with console.capture() as capture:
        console.print(issue_list)

    assert capture.get() == "\n".join(
        [
            "IssueList(",
            "    [",
            "        Issue(",
            "            type='issue',",
            "            check_name='pytest-issue-1',",
            "            description='pytest issue #1',",
            "            content=None,",
            "            categories=None,",
            "            location=IssueLocationLineRange(",
            "                path='test/unit/models/validation/test_code_climate.py',",
            "                lines=LineRange(begin=123, end=456)",
            "            ),",
            "            other_locations=None,",
            "            trace=None,",
            "            remediation_points=None,",
            "            severity='major',",
            "            fingerprint='6ef37ccc3cfae24c'",
            "        ),",
            "        Issue(",
            "            type='issue',",
            "            check_name='pytest-issue-2',",
            "            description='pytest issue #2',",
            "            content=None,",
            "            categories=None,",
            "            location=IssueLocationLineRange(",
            "                path='test/unit/models/validation/test_code_climate.py',",
            "                lines=LineRange(begin=457, end=890)",
            "            ),",
            "            other_locations=None,",
            "            trace=None,",
            "            remediation_points=None,",
            "            severity='major',",
            "            fingerprint='d81bac27c7c0dc3c'",
            "        ),",
            "        Issue(",
            "            type='issue',",
            "            check_name='pytest-issue-3',",
            "            description='pytest issue #3',",
            "            content=None,",
            "            categories=None,",
            "            location=IssueLocationLineRange(",
            "                path='test/unit/models/validation/test_code_climate.py',",
            "                lines=LineRange(begin=999, end=999)",
            "            ),",
            "            other_locations=None,",
            "            trace=None,",
            "            remediation_points=None,",
            "            severity='major',",
            "            fingerprint='48b65c15def80ec2'",
            "        )",
            "    ]",
            ")",
            "",
        ]
    )
