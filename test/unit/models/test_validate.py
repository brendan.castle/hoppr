"""
Test module for `hopctl validate sbom` pydantic models
"""

from __future__ import annotations

import pytest

from hoppr.models.validate import (
    ValidateCheckResult,
    ValidateComponentResult,
    ValidateLicenseResult,
    ValidateSbomResult,
)


@pytest.mark.parametrize(
    argnames=["result", "other", "expected_result"],
    argvalues=[
        pytest.param(
            ValidateSbomResult(
                name="pytest.cdx.json",
                component_results=[ValidateComponentResult(component_id="equal")],
                license_results=[ValidateLicenseResult(license_id="equal")],
                ntia_fields_result=ValidateCheckResult.fail("equal"),
                result=ValidateCheckResult.fail("equal"),
                spec_version_result=ValidateCheckResult.success(),
            ),
            ValidateSbomResult(
                name="pytest.cdx.json",
                component_results=[ValidateComponentResult(component_id="equal")],
                license_results=[ValidateLicenseResult(license_id="equal")],
                ntia_fields_result=ValidateCheckResult.fail("equal"),
                result=ValidateCheckResult.fail("equal"),
                spec_version_result=ValidateCheckResult.success(),
            ),
            True,
            id="equal",
        ),
        pytest.param(
            ValidateSbomResult(
                name="pytest.cdx.json",
                component_results=[ValidateComponentResult(component_id="equal")],
                license_results=[ValidateLicenseResult(license_id="equal")],
                ntia_fields_result=ValidateCheckResult.fail("equal"),
                result=ValidateCheckResult.fail("equal"),
                spec_version_result=ValidateCheckResult.success(),
            ),
            ValidateSbomResult(
                name="pytest.cdx.json",
                component_results=[ValidateComponentResult(component_id="equal")],
                license_results=[ValidateLicenseResult(license_id="equal")],
                ntia_fields_result=ValidateCheckResult.success("not equal"),
                result=ValidateCheckResult.fail("equal"),
                spec_version_result=ValidateCheckResult.success(),
            ),
            False,
            id="not-equal",
        ),
    ],
)
def test_validate_sbom_result__eq__(
    result: ValidateSbomResult,
    other: ValidateSbomResult,
    expected_result: bool,
):
    """
    Test ValidateSbomResult.__eq__ method
    """
    assert (result == other) == expected_result
