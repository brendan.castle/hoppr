"""
Test module for credentials pydantic models
"""
from pathlib import Path

import pytest

from pydantic import SecretStr, ValidationError
from pytest import MonkeyPatch

from hoppr.models.credentials import CredentialRequiredService, Credentials

credentials_dict = {
    "kind": "Credentials",
    "metadata": {"name": "Registry Credentials", "version": "v1", "description": "Sample credentials file"},
    "schemaVersion": "v1",
    "credential_required_services": [
        {"url": "some-site.com", "user": "pipeline-bot", "user_env": None, "pass_env": "SOME_TOKEN"},
        {"url": "registry-test.com", "user": "cis-gitlab", "user_env": None, "pass_env": "DOCKER_PW"},
        {"url": "gitlab.com", "user": "infra-pipeline-auto", "user_env": None, "pass_env": "GITLAB_PW"},
    ],
}


@pytest.fixture(autouse=True)
def set_environment_vars(monkeypatch: MonkeyPatch):
    """
    Fixture to set environment variables for testing
    """
    monkeypatch.setenv(name="SOME_USERNAME", value="pipeline-bot")
    monkeypatch.setenv(name="SOME_TOKEN", value="SoMe_ToKeN1234!@#$")
    monkeypatch.setenv(name="DOCKER_PW", value="DoCKeR_PW1234!@#$")
    monkeypatch.setenv(name="DOCKER_ADMIN_PW", value="AdminDockerPW")
    monkeypatch.setenv(name="GITLAB_PW", value="GiTLaB_PW1234!@#$")


def test_credentials_find(resources_dir: Path):
    """
    Test Credentials.find class method
    """
    credentials_file = resources_dir / "credential" / "cred-test.yml"

    credentials = Credentials.load(credentials_file)
    assert credentials

    assert isinstance(Credentials.find(url="some-site.com"), CredentialRequiredService)
    assert isinstance(Credentials.find(url="registry-test.com"), CredentialRequiredService)
    assert isinstance(Credentials.find(url="registry-test.com/something"), CredentialRequiredService)
    assert isinstance(Credentials.find(url="registry-test.com/admin"), CredentialRequiredService)
    assert isinstance(Credentials.find(url="registry-test.com/admin/stuff"), CredentialRequiredService)
    assert isinstance(Credentials.find(url="gitlab.com"), CredentialRequiredService)
    assert Credentials.find(url="gitlab") is None


def test_load_credentials_dict(monkeypatch: MonkeyPatch):
    """
    Test Credentials.load method with dict input
    """
    with monkeypatch.context() as patch:
        patch_dict = credentials_dict
        patch_dict["credential_required_services"] = [
            {"url": "gitlab.com", "user_env": "GITLAB_USER", "pass_env": "GITLAB_PW"}
        ]

        patch.setenv(name="GITLAB_USER", value="gitlab_test_user")
        patch.setenv(name="GITLAB_PW", value="GiTLaB_PW1234!@#$")

        credentials = Credentials.load(credentials_dict)
        assert credentials is not None

        found_creds = Credentials.find("gitlab.com")
        assert found_creds is not None
        assert found_creds.username == "gitlab_test_user"
        assert found_creds.password
        if isinstance(found_creds.password, SecretStr):
            assert found_creds.password.get_secret_value() == "GiTLaB_PW1234!@#$"


@pytest.mark.parametrize(
    argnames=["repo_url", "expected_user", "expected_password"],
    argvalues=[
        pytest.param("some-site.com", "pipeline-bot", "SoMe_ToKeN1234!@#$"),
        pytest.param("registry-test.com", "cis-gitlab", "DoCKeR_PW1234!@#$"),
        pytest.param("registry-test.com/admin", "docker_admin", "AdminDockerPW"),
        pytest.param("gitlab.com", "infra-pipeline-auto", "GiTLaB_PW1234!@#$"),
    ],
)
def test_load_credentials_file(repo_url: str, expected_user: str, expected_password: str, resources_dir: Path):
    """
    Test Credentials.load method with Path input
    """
    credentials_file = resources_dir / "credential" / "cred-test.yml"

    credentials = Credentials.load(credentials_file)
    assert credentials

    # Test __getitem__ method (indexed lookup on Credentials instance)
    repo_credentials = credentials[repo_url]

    assert repo_credentials.username == expected_user
    assert repo_credentials.password
    assert isinstance(repo_credentials.password, SecretStr)
    assert repo_credentials.password.get_secret_value() == expected_password


@pytest.mark.parametrize(argnames="missing_var", argvalues=["SOME_USERNAME", "SOME_TOKEN"])
def test_load_credentials_missing_environment_var(missing_var: str, resources_dir: Path, monkeypatch: MonkeyPatch):
    """
    Test Credentials.load method with missing environment variables
    """
    credentials_file = resources_dir / "credential" / "cred-test.yml"

    with monkeypatch.context() as patch:
        patch.setenv(name=missing_var, value="")

        with pytest.raises(
            expected_exception=ValidationError,
            match=f".*The environment variable {missing_var} must be set with a non-empty string.*",
        ):
            Credentials.load(credentials_file)


def test_load_credentials_none():
    """
    Test Credentials.load method with None input
    """
    credentials = Credentials.load(None)
    assert credentials is None


def test_load_credentials_wrong_type():
    """
    Test Credentials.load method with wrong input type
    """
    with pytest.raises(expected_exception=TypeError):
        Credentials.load(0)
