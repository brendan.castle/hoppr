"""Test module for `hopctl bundle` subcommand."""

from __future__ import annotations

import re

from typing import TYPE_CHECKING, TypeAlias

import pytest

import hoppr.cli.bundle
import hoppr.main
import hoppr.processor
import hoppr.utils

from hoppr.cli import app
from hoppr.cli.bundle import HopprBundleLayout, HopprBundleSummary
from hoppr.cli.layout import console
from hoppr.models.sbom import Sbom
from hoppr.models.transfer import TransferFile


if TYPE_CHECKING:
    from pathlib import Path

    from typer.testing import CliRunner

    LogCaptureFixture: TypeAlias = pytest.LogCaptureFixture
    MonkeyPatch: TypeAlias = pytest.MonkeyPatch


@pytest.fixture
def setup_processor(monkeypatch: MonkeyPatch):
    """Fixture to do initial test setup."""
    # Environment variables required for credentials file validation
    monkeypatch.setenv(name="DOCKER_PW", value="DoCKeR_PW1234!@#$")
    monkeypatch.setenv(name="DOCKER_ADMIN_PW", value="AdminDockerPW")
    monkeypatch.setenv(name="GITLAB_PW", value="GiTLaB_PW1234!@#$")
    monkeypatch.setenv(name="SOME_USERNAME", value="pipeline-bot")
    monkeypatch.setenv(name="SOME_TOKEN", value="SoMe_ToKeN1234!@#$")

    console.width = 100
    hoppr.processor.layout = HopprBundleLayout()
    hoppr.processor.summary_panel = HopprBundleSummary()


@pytest.mark.usefixtures("setup_processor")
def test_hopctl_bundle_delivered_sbom_output(hopctl_runner: CliRunner, resources_dir: Path, tmp_path: Path):
    """Test `hopctl bundle` and assert expected content of delivered SBOM."""
    transfer_file = TransferFile(kind="Transfer", schema_version="v1", stages={})

    # Write temporary transfer file with empty `stages` to bypass processing in `HopprProcessor.run`
    (tmp_path / "transfer.yml").write_text(transfer_file.yaml(by_alias=True, indent=True))

    result = hopctl_runner.invoke(
        app,
        args=[
            "bundle",
            f"--credentials={resources_dir / 'credential' / 'cred-test.yml'}",
            f"--transfer={tmp_path / 'transfer.yml'}",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
            f"--delivered-sbom-output={tmp_path / 'pytest-sbom.cdx.json'}",
            f"{resources_dir / 'manifest' / 'unit' / 'manifest.yml'}",
            "--verbose",
            "--basic-term",
        ],
        catch_exceptions=False,
    )

    assert result.exit_code == 0
    assert (tmp_path / "pytest-sbom.cdx.json").exists()
    assert isinstance(Sbom.load(tmp_path / "pytest-sbom.cdx.json"), Sbom)


def test_hopctl_bundle_missing_functionary_key(hopctl_runner: CliRunner, resources_dir: Path, tmp_path: Path):
    """Test `hopctl bundle --attest` raises expected exception."""
    result = hopctl_runner.invoke(
        app,
        args=[
            "bundle",
            f"--credentials={resources_dir / 'credential' / 'cred-test.yml'}",
            f"--transfer={tmp_path / 'transfer.yml'}",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
            f"--delivered-sbom-output={tmp_path / 'pytest-sbom.cdx.json'}",
            f"{resources_dir / 'manifest' / 'manifest.yml'}",
            "--attest",
            "--verbose",
            "--basic-term",
        ],
        catch_exceptions=False,
    )

    output = re.sub(pattern=r"╭.*╮|╰.*╯|│|\n|\s{2,}", repl="", string=result.stdout)

    assert result.exit_code == 2
    assert (
        "Invalid value for '-a' / '--attest': To create attestations both the"
        " --attest option and a functionary private key must be provided."
    ) in output


@pytest.mark.parametrize(
    argnames=["func_key_prompt", "expected_password"],
    argvalues=[
        pytest.param(True, "p@$$w0rd", id="with-prompt"),
        pytest.param(False, None, id="without-prompt"),
    ],
)
def test_hopctl_bundle_functionary_key_prompt(  # pylint: disable=too-many-arguments
    func_key_prompt: bool,
    expected_password: str | None,
    hopctl_runner: CliRunner,
    resources_dir: Path,
    monkeypatch: MonkeyPatch,
    tmp_path: Path,
):
    """Test `hopctl bundle --attest` with functionary key password prompt."""

    def _prompt_patch(*_, **__) -> None:
        print(expected_password, end="")

    monkeypatch.setattr(target=hoppr.cli.bundle, name="prompt", value=_prompt_patch)
    monkeypatch.setattr(target=hoppr.main, name="bundle", value=lambda *_, **__: None)

    result = hopctl_runner.invoke(
        app,
        args=[
            "bundle",
            f"--credentials={resources_dir / 'credential' / 'cred-test.yml'}",
            f"--transfer={tmp_path / 'transfer.yml'}",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
            f"{resources_dir / 'manifest' / 'manifest.yml'}",
            "--attest",
            f"--functionary-key={tmp_path / 'func-key'}",
            *(["--prompt"] if func_key_prompt else []),
            "--verbose",
            "--basic-term",
        ],
        catch_exceptions=False,
    )

    assert result.exit_code == 0
    assert result.stdout == (expected_password or "")
