"""
Test module for `hopctl validate` subcommand
"""

# pylint: disable=protected-access

from __future__ import annotations

import importlib
import json
import re

from datetime import datetime, timedelta
from pathlib import Path
from typing import TYPE_CHECKING, Callable, TypeAlias

import pytest

from rich.text import Text
from typer import BadParameter, Context
from typer.core import TyperCommand

import hoppr.cli.experimental.validate
import hoppr.cli.options
import hoppr.models.validation
import hoppr.net
import hoppr.utils

from hoppr.cli import app, validate
from hoppr.cli.layout import HopprLayout
from hoppr.cli.validate import HopprValidateSummary
from hoppr.models.licenses import LicenseExpressionItem, LicenseMultipleItem, Licensing, NamedLicense
from hoppr.models.sbom import Component, ComponentType, Metadata
from hoppr.models.validate import (
    ValidateCheckResult,
    ValidateComponentResult,
    ValidateLicenseResult,
    ValidateSbomResult,
)
from hoppr.models.validation.checks import ValidateConfig
from hoppr.models.validation.code_climate import IssueList


if TYPE_CHECKING:
    from typer.testing import CliRunner

    FixtureRequest: TypeAlias = pytest.FixtureRequest
    LogCaptureFixture: TypeAlias = pytest.LogCaptureFixture
    MonkeyPatch: TypeAlias = pytest.MonkeyPatch


def _parse_results_file(results_file: Path) -> list[ValidateSbomResult]:
    results_data = json.loads(results_file.read_text(encoding="utf-8"))
    return [ValidateSbomResult(**result) for result in results_data]


@pytest.fixture(autouse=True)
def reset_environment(monkeypatch: MonkeyPatch):
    """Reset the `HOPPR_EXPERIMENTAL` environment variable before each test in this module."""
    monkeypatch.delenv(name="HOPPR_EXPERIMENTAL", raising=False)


@pytest.fixture(name="component")
def component_fixture(request: FixtureRequest) -> Component:
    """
    Test Component fixture
    """
    param_dict = dict(getattr(request, "param", {}))

    param_dict["name"] = param_dict.get("name", "test-component")
    param_dict["purl"] = param_dict.get("purl", "pkg:pypi/test-component@1.2.3")
    param_dict["type"] = param_dict.get("type", "library")

    return Component(**param_dict)


@pytest.fixture
def init_globals_fixture(request: FixtureRequest, tmp_path: Path) -> None:
    """
    Fixture to initialize globals in the `hoppr.cli.validate` module
    """
    param_dict = dict(getattr(request, "param", {}))

    # Reload `hoppr.cli.validate` module to reset globals
    importlib.reload(validate)

    validate._init_globals(
        log_file=Path(tmp_path / "hoppr-pytest.log"),
        verbose=True,
        expiration_days=param_dict.get("expiration_days", 30),
        strict=param_dict.get("strict", False),
        strict_license_fields=param_dict.get("strict_license_fields", False),
        strict_ntia_minimum_fields=param_dict.get("strict_ntia_minimum_fields", False),
    )


@pytest.fixture(name="license_expression")
def license_expression_fixture(request: FixtureRequest) -> LicenseExpressionItem | None:
    """
    Fixture to return license with expression
    """
    if not (param_dict := getattr(request, "param", {})):
        return None

    return LicenseExpressionItem.parse_obj({"expression": param_dict.get("expression", "MIT")})


@pytest.fixture(name="named_license")
def named_license_fixture(request: FixtureRequest) -> LicenseMultipleItem | None:
    """
    Fixture to return SPDX or named license
    """
    if not (param_dict := getattr(request, "param", {})):
        return None

    return LicenseMultipleItem.parse_obj({"license": param_dict})


@pytest.mark.parametrize(
    argnames=["profile", "expected_result", "want_error"],
    argvalues=[
        pytest.param(
            "test",
            "test",
            True,
            id="unknown-profile",
        ),
        pytest.param(
            None,
            "default",
            False,
            id="none-profile",
        ),
        pytest.param(
            "ntia",
            "ntia",
            False,
            id="good-profile",
        ),
    ],
)
def test__profile_callback(
    profile: str,
    expected_result: str,
    want_error: bool,
):
    """
    Test `hoppr.cli.validate._profile_callback` function
    """

    if want_error:
        with pytest.raises(BadParameter) as e:
            result = validate._profile_callback(profile)

            assert e.type == BadParameter
    else:
        result = validate._profile_callback(profile)

        assert result == expected_result


@pytest.mark.parametrize(
    argnames=["config_file", "expected_result", "exists"],
    argvalues=[
        pytest.param(
            Path("some/other/path/hoppr.config.yml"),
            Path("some/other/path/hoppr.config.yml"),
            [False, True],
            id="user-supplied-path",
        ),
        pytest.param(
            None,
            Path(hoppr.models.validation.__file__).parent / "profiles" / "default.config.yml",
            [False, False],
            id="no-path-no-local-configs",
        ),
        pytest.param(
            None,
            Path.home() / ".config" / "hoppr.config.yml",
            [True],
            id="no-path-one-local-config",
        ),
        pytest.param(
            None,
            Path.cwd() / "hoppr.config.yml",
            [False, True],
            id="other-local-config",
        ),
    ],
)
def test__config_file_callback(
    config_file: Path,
    expected_result: Path,
    exists: list[bool],
    monkeypatch: MonkeyPatch,
):
    """
    Test `hoppr.cli.validate._config_file_callback` function
    """
    exists_iter = iter(exists)

    def _exists_patch(_self: Path) -> bool:
        return next(exists_iter)

    monkeypatch.setattr(target=Path, name="exists", value=_exists_patch)
    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=lambda _path: {})

    ctx = Context(command=TyperCommand(name="hopctl validate sbom"))
    ctx.params = {"profile": "default"}

    result = validate._config_file_callback(ctx, config_file)

    assert result == expected_result


@pytest.mark.usefixtures("init_globals_fixture")
@pytest.mark.parametrize(
    argnames=["fields", "expected_result", "init_globals_fixture"],
    argvalues=[
        pytest.param(
            ("purl",),
            ValidateCheckResult.success(),
            {"strict_ntia_minimum_fields": False},
            id="single-field-success",
        ),
        pytest.param(
            ("supplier",),
            ValidateCheckResult.warn(),
            {"strict_ntia_minimum_fields": False},
            id="single-field-warn",
        ),
        pytest.param(
            ("supplier",),
            ValidateCheckResult.fail(),
            {"strict_ntia_minimum_fields": True},
            id="single-field-fail",
        ),
        pytest.param(
            ("name", "purl", "type"),
            ValidateCheckResult.success(),
            {"strict_ntia_minimum_fields": False},
            id="multiple-field-success",
        ),
        pytest.param(
            ("licenses", "supplier", "cpe"),
            ValidateCheckResult.warn(),
            {"strict_ntia_minimum_fields": False},
            id="multiple-field-warn",
        ),
        pytest.param(
            ("licenses", "supplier", "cpe"),
            ValidateCheckResult.fail(),
            {"strict_ntia_minimum_fields": True},
            id="multiple-field-fail",
        ),
    ],
    indirect=["init_globals_fixture"],
)
def test__check_fields(component: Component, fields: tuple[str, ...], expected_result: ValidateCheckResult):
    """
    Test `hoppr.cli.validate._check_fields` function
    """
    result = validate._check_fields(
        component, *fields, fail_log_level=validate.NTIA_FAIL_LOG_LEVEL, fail_result=validate.NTIA_FAIL_RESULT
    )

    assert result == expected_result


@pytest.mark.usefixtures("init_globals_fixture")
@pytest.mark.parametrize(
    argnames=["expiration", "expected_result", "init_globals_fixture"],
    argvalues=[
        pytest.param(
            "2999-12-31T00:00:00",
            ValidateCheckResult.success(),
            {"strict_license_fields": True},
            id="not-expired-str",
        ),
        pytest.param(
            datetime(year=2999, month=12, day=31),
            ValidateCheckResult.success(),
            {"strict_license_fields": True},
            id="not-expired-datetime",
        ),
        pytest.param(
            None,
            ValidateCheckResult.warn(),
            {"strict_license_fields": False},
            id="no-license-expiration",
        ),
        pytest.param(
            None,
            ValidateCheckResult.fail(),
            {"strict_license_fields": True},
            id="no-license-expiration-strict",
        ),
        pytest.param(
            datetime.now() + timedelta(days=15),
            ValidateCheckResult.warn(),
            {"strict_license_fields": False},
            id="expiring-soon",
        ),
        pytest.param(
            datetime.now() + timedelta(days=15),
            ValidateCheckResult.fail(),
            {"strict_license_fields": True},
            id="expiring-soon-strict",
        ),
    ],
    indirect=["init_globals_fixture"],
)
def test__check_license_expiration(expiration: datetime | None, expected_result: ValidateCheckResult):
    """
    Test `hoppr.cli.validate._check_license_expiration` function
    """
    result = validate._check_license_expiration(
        LicenseMultipleItem(license=NamedLicense(name="pytest", licensing=Licensing(expiration=expiration)))
    )

    assert result == expected_result


@pytest.mark.usefixtures("init_globals_fixture")
@pytest.mark.parametrize(
    argnames=["base_obj", "expected_result"],
    argvalues=[
        pytest.param(
            Metadata(),
            [
                ValidateLicenseResult(
                    license_id="Missing license data",
                    expiration=ValidateCheckResult.warn("License Expiration"),
                    required_fields=ValidateCheckResult.warn("Required License Fields"),
                )
            ],
            id="metadata-no-licenses",
        ),
        pytest.param(
            Metadata(licenses=[LicenseMultipleItem.parse_obj({"license": {"id": "MIT"}})]),
            [
                ValidateLicenseResult(
                    license_id="MIT",
                    required_fields=ValidateCheckResult.warn("Required License Fields"),
                )
            ],
            id="metadata-with-licenses",
        ),
        pytest.param(
            None,
            [
                ValidateLicenseResult(
                    license_id="Missing license data",
                    expiration=ValidateCheckResult.warn("License Expiration"),
                    required_fields=ValidateCheckResult.warn("Required License Fields"),
                )
            ],
            id="metadata-missing",
        ),
        pytest.param(
            Component(type=ComponentType.LIBRARY, name="test-component", version="1.2.3"),
            [
                ValidateLicenseResult(
                    license_id="Missing license data",
                    expiration=ValidateCheckResult.warn("License Expiration"),
                    required_fields=ValidateCheckResult.warn("Required License Fields"),
                )
            ],
            id="component-no-licenses",
        ),
        pytest.param(
            Component(
                type=ComponentType.LIBRARY,
                name="test-component",
                version="1.2.3",
                licenses=[LicenseMultipleItem.parse_obj({"license": {"name": "MIT"}})],
            ),
            [
                ValidateLicenseResult(
                    license_id="MIT",
                    required_fields=ValidateCheckResult.warn("Required License Fields"),
                )
            ],
            id="component-with-licenses",
        ),
    ],
)
def test__check_license_fields(
    base_obj: Metadata | Component | None,
    expected_result: list[ValidateLicenseResult],
    monkeypatch: MonkeyPatch,
):
    """
    Test `hoppr.cli.validate._check_license_fields` function
    """
    monkeypatch.setattr(
        target=validate,
        name="_check_license_expiration",
        value=lambda *_, **__: ValidateCheckResult.success(),
    )

    assert validate._validate_licenses(base_obj) == expected_result


@pytest.mark.parametrize(
    argnames=["spec_version", "strict_all", "expected_result"],
    argvalues=[
        pytest.param("1.2", False, ValidateCheckResult.fail(), id="1.2"),
        pytest.param("1.3", False, ValidateCheckResult.warn(), id="1.3"),
        pytest.param("1.4", False, ValidateCheckResult.warn(), id="1.4"),
        pytest.param("1.5", False, ValidateCheckResult.success(), id="1.5"),
        pytest.param("1.2", True, ValidateCheckResult.fail(), id="1.2-strict"),
        pytest.param("1.3", True, ValidateCheckResult.fail(), id="1.3-strict"),
        pytest.param("1.4", True, ValidateCheckResult.fail(), id="1.4-strict"),
        pytest.param("1.5", True, ValidateCheckResult.success(), id="1.5-strict"),
    ],
)
def test__check_spec_version(
    spec_version: str,
    strict_all: bool,
    expected_result: ValidateCheckResult,
    monkeypatch: MonkeyPatch,
):
    """
    Test `hoppr.cli.validate._check_spec_version` function
    """
    monkeypatch.setattr(target=validate, name="STRICT_ALL", value=strict_all)

    result = validate._check_spec_version({"bomFormat": "CycloneDX", "specVersion": spec_version})

    assert result == expected_result


@pytest.mark.parametrize(
    argnames=["manifest_file", "sbom_files", "sbom_urls", "load_url_fixture", "load_file_fixture", "expected_error"],
    argvalues=[
        pytest.param(
            Path("test/integration/yum/manifest.yml"),
            [Path("test/integration/dnf/dnf-bom.json")],
            ["https://gitlab.com/hoppr/hoppr/-/raw/dev/test/integration/gem/gem-bom.json"],
            {"super": "duper"},
            {"also": "superduper"},
            "",
            id="success",
        ),
        pytest.param(
            Path("test/integration/yum/manifest.yml"),
            [Path("test/integration/dnf/dnf-bom.json")],
            ["https://gitlab.com/hoppr/hoppr/-/raw/dev/test/integration/gem/gem-bom.json"],
            {"super": "duper"},
            None,
            "SBOM file was not loaded as dictionary",
            id="invalid-file",
        ),
        pytest.param(
            Path("test/integration/yum/manifest.yml"),
            [Path("test/integration/dnf/dnf-bom.json")],
            ["this.one.is.wrong"],
            None,
            {"super": "duper"},
            "SBOM URL was not loaded as dictionary",
            id="invalid-url",
        ),
    ],
    indirect=["load_url_fixture", "load_file_fixture"],
)
def test__create_sbom_data_map(  # pylint: disable=too-many-arguments
    manifest_file: Path,
    sbom_files: list[Path],
    sbom_urls: list[str],
    load_url_fixture: Callable[[str], object],
    load_file_fixture: Callable[[Path], object],
    expected_error: str,
    monkeypatch: MonkeyPatch,
):
    """
    Test `hoppr.cli.validate._create_sbom_data_map` function
    """
    monkeypatch.setattr(target=hoppr.net, name="load_url", value=load_url_fixture)
    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=load_file_fixture)

    try:
        result = validate._create_sbom_data_map(manifest_file, sbom_files, sbom_urls)

        assert not expected_error
        assert result
    except TypeError:
        assert expected_error


def test_hopctl_validate_sbom(hopctl_runner: CliRunner, resources_dir: Path, tmp_path: Path, monkeypatch: MonkeyPatch):
    """
    Test `hopctl validate sbom` subcommand
    """
    monkeypatch.setattr(target=hoppr.utils, name="is_basic_terminal", value=lambda: False)

    # Patch `HopprLayout.job_id_map` to force sync between mapping and jobs panel tasks
    monkeypatch.setattr(target=HopprLayout, name="job_id_map", value={})

    result = hopctl_runner.invoke(
        app,
        args=[
            "validate",
            "sbom",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
            "--sbom=test/resources/validate/sboms/validate-test-invalid.cdx.json",
            "--output-format=json",
            f"--output-file={tmp_path / 'results.json'}",
        ],
    )

    expected_results = _parse_results_file(resources_dir / "validate" / "expected-results-no-strict.json")

    assert result.exit_code == 0
    assert _parse_results_file(tmp_path / "results.json") == expected_results


@pytest.mark.parametrize(
    argnames=["sbom_id"],
    argvalues=[
        pytest.param("expiration", id="expiration"),
        pytest.param("last-renewal", id="last-renewal"),
        pytest.param("licenses-field", id="licenses-field"),
        pytest.param("licenses-types", id="licenses-types"),
        pytest.param("metadata-authors", id="metadata-authors"),
        pytest.param("metadata-supplier", id="metadata-supplier"),
        pytest.param("metadata-timestamp", id="metadata-timestamp"),
        pytest.param("name-field", id="name-field"),
        pytest.param("name-or-id", id="name-or-id"),
        pytest.param("purchase-order", id="purchase-order"),
        pytest.param("sbom-components", id="sbom-components"),
        pytest.param("sbom-spec-version", id="sbom-spec-version"),
        pytest.param("sbom-unique-id", id="sbom-unique-id"),
        pytest.param("sbom-vulnerabilities-field", id="sbom-vulnerabilities-field"),
        pytest.param("supplier-field", id="supplier-field"),
        pytest.param("unique-id", id="unique-id"),
        pytest.param("valid", id="valid"),
        pytest.param("version-field", id="version-field"),
    ],
)
def test_hopctl_validate_sbom_experimental(
    hopctl_runner: CliRunner,
    sbom_id: str,
    resources_dir: Path,
    monkeypatch: MonkeyPatch,
    tmp_path: Path,
):
    """Test `hopctl validate sbom` subcommand with `--experimental` option."""
    validate_resources_dir = resources_dir.relative_to(Path.cwd()) / "validate"
    sbom_file = validate_resources_dir / "sboms" / f"validate-test-{sbom_id}.cdx.json"
    expected = IssueList.parse_file(validate_resources_dir / "code_climate" / f"expected-issues-{sbom_id}.json")

    args = [
        "validate",
        "sbom",
        f"--log={tmp_path / 'hoppr-pytest.log'}",
        f"--sbom={sbom_file}",
        f"--output-file={tmp_path / 'results.json'}",
        "--experimental",
    ]

    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.cli.experimental.validate, name="_EXIT_CODE", value=0)
        patch.delattr(target=ValidateConfig, name="__instance__", raising=False)

        result = hopctl_runner.invoke(app, args=args)

        assert result.exit_code == 0
        assert IssueList.parse_file(tmp_path / "results.json") == expected

    # Add `--profile strict` and assert failing exit code
    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.cli.experimental.validate, name="_EXIT_CODE", value=0)
        patch.delattr(target=ValidateConfig, name="__instance__", raising=False)

        args += ["--profile", "strict"]
        result = hopctl_runner.invoke(app, args=args)

        assert result.exit_code == 0 if sbom_id == "valid" else 1


def test_hopctl_validate_sbom_experimental_full_terminal(
    hopctl_runner: CliRunner,
    resources_dir: Path,
    tmp_path: Path,
    monkeypatch: MonkeyPatch,
):
    """Test `hopctl validate sbom` subcommand with `--experimental` option."""
    monkeypatch.setattr(target=hoppr.utils, name="is_basic_terminal", value=lambda: False)
    monkeypatch.setattr(target=hoppr.cli.experimental.validate, name="_EXIT_CODE", value=0)

    validate_resources_dir = resources_dir.relative_to(Path.cwd()) / "validate"
    sbom_file = validate_resources_dir / "sboms" / "validate-test-valid.cdx.json"
    expected = IssueList.parse_file(validate_resources_dir / "code_climate" / "expected-issues-valid.json")

    result = hopctl_runner.invoke(
        app,
        args=[
            "validate",
            "sbom",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
            f"--sbom={sbom_file}",
            f"--output-file={tmp_path / 'results.json'}",
            "--experimental",
        ],
    )

    assert result.exit_code == 0
    assert IssueList.parse_file(tmp_path / "results.json") == expected


def test_hopctl_validate_sbom_experimental_sbom_urls(
    hopctl_runner: CliRunner,
    resources_dir: Path,
    tmp_path: Path,
    monkeypatch: MonkeyPatch,
):
    """Test `hopctl validate sbom` subcommand with `--experimental` and `--sbom-url` options."""
    validate_resources_dir = resources_dir.relative_to(Path.cwd()) / "validate"
    sbom_file = validate_resources_dir / "sboms" / "validate-test-valid.cdx.json"

    def _download_file_patch(dest: str, *_, **__):
        content = sbom_file.read_text(encoding="utf-8")
        Path(dest).write_text(data=content, encoding="utf-8")

    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.cli.experimental.validate, name="_EXIT_CODE", value=0)
        patch.delattr(target=ValidateConfig, name="__instance__", raising=False)
        patch.setattr(target=hoppr.net, name="download_file", value=_download_file_patch)

        result = hopctl_runner.invoke(
            app,
            args=[
                "validate",
                "sbom",
                f"--log={tmp_path / 'hoppr-pytest.log'}",
                "--sbom-url=https://example.acme.com/sboms/sbom.cdx.json",
                f"--output-file={tmp_path / 'results.json'}",
                "--experimental",
            ],
        )

        assert result.exit_code == 0


def test_hopctl_validate_sbom_load_file_fail(hopctl_runner: CliRunner, tmp_path: Path, monkeypatch: MonkeyPatch):
    """
    Test `hopctl validate sbom` subcommand fails if SBOM file is not loaded as `dict`
    """
    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=lambda path: [])

    result = hopctl_runner.invoke(
        app,
        args=[
            "validate",
            "sbom",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
            "--sbom=test/resources/bom/unit_bom1_mini.json",
            "--basic-term",
        ],
    )

    assert result.exit_code == 1


def test_hopctl_validate_sbom_logging_validation_error(
    hopctl_runner: CliRunner,
    tmp_path: Path,
    caplog: LogCaptureFixture,
    monkeypatch: MonkeyPatch,
):
    """
    Test logging output of `hopctl validate sbom` subcommand if a pydantic `ValidationError` is raised
    """
    # Set logger back to None so a new one gets created
    monkeypatch.setattr(target=hoppr.cli.options, name="_logger", value=None)

    sbom_data = {
        "bomFormat": "_CycloneDX_",  # Must be Literal["CycloneDX"]
        "specVersion": "1.5",
        "components": [{"name": "test-component", "version": "0.0.1"}],  # Missing `type` field
    }

    (tmp_path / "bad-sbom.cdx.json").write_text(data=json.dumps(sbom_data), encoding="utf-8")

    result = hopctl_runner.invoke(
        app,
        args=[
            "validate",
            "sbom",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
            f"--sbom={tmp_path / 'bad-sbom.cdx.json'}",
            "--output-format=json",
            f"--output-file={tmp_path / 'results.json'}",
            "--verbose",
        ],
        catch_exceptions=False,
    )

    output = re.sub(pattern=r"\s+│\n│\s", repl="", string=result.stdout)

    assert result.exit_code == 2
    assert "Invalid value for '-s' / '--sbom':" in output
    assert f"'{tmp_path / 'bad-sbom.cdx.json'}'" in output

    # Verify expected log output
    expected_log_message = f"""'{tmp_path / 'bad-sbom.cdx.json'}' failed to validate against the CycloneDX schema:
    2 validation errors for Sbom
    bomFormat
      unexpected value; permitted: 'CycloneDX' (type=value_error.const; given=_CycloneDX_; permitted=('CycloneDX',))
    components -> type
      field required (type=value_error.missing)"""

    *_, last_message = caplog.messages
    assert last_message == expected_log_message


@pytest.mark.parametrize(
    argnames=["strict_flag", "expected_results_file"],
    argvalues=[
        pytest.param("--strict-ntia-minimum-fields", "expected-results-strict-ntia.json", id="strict-ntia"),
        pytest.param("--strict-license-fields", "expected-results-strict-license.json", id="strict-license"),
        pytest.param("--strict", "expected-results-strict-all.json", id="strict-all"),
    ],
)
def test_hopctl_validate_sbom_strict(
    strict_flag: str,
    expected_results_file: str,
    hopctl_runner: CliRunner,
    resources_dir: Path,
    tmp_path: Path,
):
    """
    Test `hopctl validate sbom` subcommand with `--strict*` options
    """
    result = hopctl_runner.invoke(
        app,
        args=[
            "validate",
            "sbom",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
            "--sbom=test/resources/validate/sboms/validate-test-invalid.cdx.json",
            "--output-format=json",
            f"--output-file={tmp_path / 'results.json'}",
            "--basic-term",
            strict_flag,
        ],
    )

    expected_results = _parse_results_file(resources_dir / "validate" / expected_results_file)

    assert result.exit_code == 1
    assert _parse_results_file(tmp_path / "results.json") == expected_results


def test_hopctl_validate_sbom_strict_small_bom(hopctl_runner: CliRunner, tmp_path: Path, monkeypatch: MonkeyPatch):
    """
    Test `hopctl validate sbom` with small SBOM file against static `ValidateSbomResult`
    """
    # Patch `HopprLayout.job_id_map` to force sync between mapping and jobs panel tasks
    monkeypatch.setattr(target=HopprLayout, name="job_id_map", value={})

    result = hopctl_runner.invoke(
        app,
        args=[
            "validate",
            "sbom",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
            "--sbom=test/resources/bom/unit_bom1_mini.json",
            "--output-format=json",
            f"--output-file={tmp_path / 'results.json'}",
            "--basic-term",
            "--strict-license-fields",
        ],
    )

    assert result.exit_code == 1

    expected_component_result = ValidateCheckResult.fail(
        "\n".join(["Minimum NTIA Fields", "Required License Fields", "License Expiration"])
    )

    expected_license_result = ValidateLicenseResult(
        license_id="Missing license data",
        expiration=ValidateCheckResult.fail("License Expiration"),
        required_fields=ValidateCheckResult.fail("Required License Fields"),
    )

    expected_ntia_fields_result = ValidateCheckResult.warn("Minimum NTIA Fields")

    expected_results = [
        ValidateSbomResult(
            name="test/resources/bom/unit_bom1_mini.json",
            component_results=[
                ValidateComponentResult(
                    component_id="@angular-devkit/architect@0.1303.1",
                    license_results=[expected_license_result],
                    ntia_fields_result=expected_ntia_fields_result,
                    result=expected_component_result,
                ),
                ValidateComponentResult(
                    component_id="@create-react-app/web-app@0.7624.1",
                    license_results=[expected_license_result],
                    ntia_fields_result=expected_ntia_fields_result,
                    result=expected_component_result,
                ),
            ],
            license_results=[expected_license_result],
            ntia_fields_result=expected_ntia_fields_result,
            result=ValidateCheckResult.fail(
                "\n".join(
                    [
                        "Minimum NTIA Fields",
                        "Required License Fields",
                        "License Expiration",
                    ]
                )
            ),
            spec_version_result=ValidateCheckResult.warn("CycloneDX Specification Version"),
        )
    ]

    assert _parse_results_file(tmp_path / "results.json") == expected_results


@pytest.mark.parametrize(
    argnames=["name", "expected_results_file"],
    argvalues=[
        pytest.param("validate-test-components.cdx.json", "expected-results-components.json", id="components"),
        pytest.param("validate-test-metadata-only.cdx.json", "expected-results-metadata-only.json", id="metadata-only"),
        pytest.param("validate-test-merged.cdx.json", "expected-results-merged.json", id="merged"),
        pytest.param("validate-test-annotations.cdx.json", "expected-results-annotations.json", id="annotations"),
    ],
)
def test_hopctl_validate_sbom_success(  # pylint: disable=too-many-arguments
    name: str,
    expected_results_file: str,
    hopctl_runner: CliRunner,
    resources_dir: Path,
    tmp_path: Path,
    monkeypatch: MonkeyPatch,
):
    """
    Test `hopctl validate sbom` with SBOM files expect to pass validation
    """
    # Patch `HopprLayout.job_id_map` to force sync between mapping and jobs panel tasks
    monkeypatch.setattr(target=HopprLayout, name="job_id_map", value={})

    result = hopctl_runner.invoke(
        app,
        args=[
            "validate",
            "sbom",
            f"--log={tmp_path / 'hoppr-pytest.log'}",
            f"--sbom=test/resources/validate/sboms/{name}",
            "--output-format=json",
            f"--output-file={tmp_path / 'results.json'}",
            "--basic-term",
            "--strict",
        ],
    )

    expected_results = _parse_results_file(resources_dir / "validate" / expected_results_file)

    assert result.exit_code == 0
    assert _parse_results_file(tmp_path / "results.json") == expected_results


@pytest.mark.usefixtures("init_globals_fixture")
@pytest.mark.parametrize(
    argnames=["expected_result", "init_globals_fixture"],
    argvalues=[
        pytest.param(Text("\u274c\ufe0f"), {"strict_ntia_minimum_fields": True}, id="strict"),
        pytest.param(Text("\u26a0\ufe0f"), {"strict_ntia_minimum_fields": False}, id="no-strict"),
    ],
    indirect=["init_globals_fixture"],
)
def test_hoppr_validate_summary__component_ntia_row(expected_result: Text):
    """
    Test `HopprValidateSummary._component_ntia_row` method
    """
    summary_panel = HopprValidateSummary(log_file=Path("test.log"))
    component_results = [
        ValidateComponentResult(component_id="test-component-1@1.2.3"),
        ValidateComponentResult(component_id="test-component-2@4.5.6", ntia_fields_result=ValidateCheckResult.fail()),
    ]

    result_row = summary_panel._component_ntia_row(component_results)

    assert result_row == ("1 out of 2 components missing minimum NTIA fields", expected_result)


@pytest.mark.usefixtures("init_globals_fixture")
@pytest.mark.parametrize(
    argnames=["expected_result", "init_globals_fixture"],
    argvalues=[
        pytest.param(Text("\u274c\ufe0f"), {"strict_license_fields": True}, id="strict"),
        pytest.param(Text("\u26a0\ufe0f"), {"strict_license_fields": False}, id="no-strict"),
    ],
    indirect=["init_globals_fixture"],
)
def test_hoppr_validate_summary__license_expiration_row(expected_result: Text):
    """
    Test `HopprValidateSummary._license_expiration_row` method
    """
    summary_panel = HopprValidateSummary(log_file=Path("test.log"))
    license_results = [
        ValidateLicenseResult(license_id="MIT"),
        ValidateLicenseResult(license_id="Apache-2.0", expiration=ValidateCheckResult.fail()),
    ]

    result_row = summary_panel._license_expiration_row(license_results, "SBOM")

    assert result_row == ("1 out of 2 SBOM licenses expired or expiring within 30 days", expected_result)


@pytest.mark.usefixtures("init_globals_fixture")
@pytest.mark.parametrize(
    argnames=["expected_result", "init_globals_fixture"],
    argvalues=[
        pytest.param(Text("\u274c\ufe0f"), {"strict_license_fields": True}, id="strict"),
        pytest.param(Text("\u26a0\ufe0f"), {"strict_license_fields": False}, id="no-strict"),
    ],
    indirect=["init_globals_fixture"],
)
def test_hoppr_validate_summary__license_fields_row(expected_result: Text):
    """
    Test `HopprValidateSummary._license_fields_row` method
    """
    summary_panel = HopprValidateSummary(log_file=Path("test.log"))
    license_results = [
        ValidateLicenseResult(license_id="MIT"),
        ValidateLicenseResult(license_id="Apache-2.0", required_fields=ValidateCheckResult.fail()),
    ]

    result_row = summary_panel._license_fields_row(license_results, "component")

    assert result_row == ("1 out of 2 component licenses missing minimum license fields", expected_result)


def test_experimental_validate__get_issues_dict():
    """
    Test `hoppr.cli.experimental.validate._get_issues_dict` method with an issue missing its check_name.
    """
    issue_list = IssueList.parse_obj(
        [
            {
                "description": "Dummy issue",
                "location": {
                    "path": "",
                    "positions": {
                        "begin": {"line": 0, "column": 0},
                        "end": {"line": 0, "column": 0},
                    },
                },
                "severity": "info",
            }
        ]
    )

    issues_dict = hoppr.cli.experimental.validate._get_issues_dict(issue_list)

    assert all(len(value) == 0 for value in issues_dict.values())
