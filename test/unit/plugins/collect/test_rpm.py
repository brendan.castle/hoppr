"""Test module for CollectRpmPlugin class."""

# pylint: disable=protected-access

from __future__ import annotations

import gzip
import textwrap

from pathlib import Path
from typing import Callable

import pytest
import requests
import xmltodict

from packageurl import PackageURL
from pytest import FixtureRequest, MonkeyPatch

import hoppr.net

from hoppr.exceptions import HopprPluginError
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import SearchSequence
from hoppr.models.sbom import Component, ComponentType, Property, Sbom
from hoppr.models.types import RepositoryUrl
from hoppr.plugins.collect.rpm import CollectRpmPlugin
from hoppr.result import Result

TEST_REPOMD_XML = textwrap.dedent(
    """
    <?xml version="1.0" encoding="UTF-8"?>
    <repomd xmlns="http://rpm.hoppr.com/metadata/repo" xmlns:rpm="http://rpm.hoppr.com/metadata/rpm">
      <revision>1667635478</revision>
      <data type="primary">
        <location href="repodata/test-primary.xml.gz"/>
      </data>
      <data type="filelists">
        <location href="repodata/test-filelists.xml.gz"/>
      </data>
      <data type="other">
        <location href="repodata/test-other.xml.gz"/>
      </data>
    </repomd>
    """
).strip("\n")

TEST_PRIMARY_XML = textwrap.dedent(
    """
    <?xml version="1.0" encoding="UTF-8"?>
    <metadata xmlns="http://rpm.hoppr.com/metadata/common" xmlns:rpm="http://rpm.hoppr.com/metadata/rpm" packages="2">
    <package type="rpm">
      <name>hoppr-test-rpm</name>
      <arch>x86_64</arch>
      <version epoch="0" ver="1.2.3" rel="1.fc37"/>
      <checksum type="sha256" pkgid="YES">ae9ade07f27932aa94a4f5c407338848c3488393cbe678ec6baa2fe86514171a</checksum>
      <url>https://github.com/hoppr/test-rpm-1</url>
      <location href="Packages/h/hoppr-test-rpm-1.2.3-1.fc37.x86_64.rpm"/>
    </package>
    <package type="rpm">
      <name>hoppr-test-rpm</name>
      <arch>x86_64</arch>
      <version epoch="1" ver="4.5.6" rel="7.fc37"/>
      <checksum type="sha256" pkgid="YES">80533ab0c74f574156ced118e6c429ba045c1697646fdee441533618e90a41f3</checksum>
      <url>https://github.com/hoppr/test-rpm-2</url>
      <location href="Packages/h/hoppr-test-rpm-4.5.6-7.fc37.x86_64.rpm"/>
    </package>
    </metadata>
    """
).strip("\n")


@pytest.fixture(name="component")
def component_fixture(pkg_url: PackageURL):
    """Fixture to return Component object from PackageURL data."""
    return Component(name=pkg_url.name, version=pkg_url.version, purl=str(pkg_url), type=ComponentType.LIBRARY)


@pytest.fixture(name="pkg_url")
def pkg_url_fixture(request: FixtureRequest) -> PackageURL:
    """Fixture to return PackageURL object."""
    param_dict = dict(getattr(request, "param", {}))

    param_dict["type"] = param_dict.get("type", "rpm")
    param_dict["namespace"] = param_dict.get("namespace", "fedora")
    param_dict["name"] = param_dict.get("name", "hoppr-test-rpm")
    param_dict["version"] = param_dict.get("version", "1.2.3-1.fc37")
    param_dict["qualifiers"] = param_dict.get("qualifiers", {"arch": "x86_64", "distro": "fedora-37", "epoch": "0"})
    param_dict["subpath"] = param_dict.get("subpath")

    return PackageURL(**param_dict)


@pytest.fixture(name="sbom")
def sbom_fixture(resources_dir: Path) -> Sbom:
    """Fixture to return an Sbom object with a repository search sequence property on each component."""
    sbom = Sbom.load(source=resources_dir / "bom" / "int_dnf_bom.json")

    search_sequence = SearchSequence(
        repositories=[
            "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os",
            "https://rpm.hoppr.com/dl/8/BaseOS/x86_64/os",
        ]
    )

    for component in sbom.components:
        component.properties.append(
            Property(
                name="hoppr:repository:component_search_sequence",
                value=search_sequence.json(),
            )
        )

    return sbom


def test__check_artifact_hash(plugin_fixture: CollectRpmPlugin, tmp_path: Path):
    """Test CollectRpmPlugin._check_artifact_hash method."""
    artifact = tmp_path / "hoppr-test-rpm-1.2.3-1.fc37.x86_64.rpm"
    artifact.write_bytes(b"0" * 1048576)

    with pytest.raises(
        expected_exception=HopprPluginError,
        match="Checksum for hoppr-test-rpm-1.2.3-1.fc37.x86_64.rpm does not match expected hash.",
    ):
        plugin_fixture._check_artifact_hash(dest_file=artifact, hash_algorithm="sha256", hash_string="invalid-checksum")


@pytest.mark.parametrize(
    argnames=["response_fixture", "expected_exception", "file_exists"],
    argvalues=[
        pytest.param(
            {"content": b"0" * 1048576},
            None,
            True,
            id="download-success",
        ),
        pytest.param(
            {"status_code": 404, "reason": "not found"},
            "HTTP Status Code: 404; not found",
            False,
            id="download-fail",
        ),
        pytest.param(
            {"status_code": 500, "reason": "server error"},
            "HTTP Status Code: 500; server error",
            False,
            id="download-retry",
        ),
    ],
    indirect=["response_fixture"],
)
def test__download_component(  # pylint: disable=too-many-arguments
    plugin_fixture: CollectRpmPlugin,
    response_fixture: Callable[[str], requests.Response],
    expected_exception: str | None,
    file_exists: bool,
    monkeypatch: MonkeyPatch,
    tmp_path: Path,
):
    """Test CollectRpmPlugin._download_component method."""
    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    download_url = "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/Packages/h/hoppr-test-rpm-1.2.3-1.fc37.x86_64.rpm"
    dest_file = tmp_path / "hoppr-test-rpm-1.2.3-1.fc37.x86_64.rpm"

    try:
        plugin_fixture._download_component(download_url, dest_file)
    except HopprPluginError as ex:
        assert str(ex) == expected_exception

    assert dest_file.is_file() == file_exists


@pytest.mark.parametrize(
    argnames=["pkg_url", "expected_download_info", "expected_exception"],
    argvalues=[
        pytest.param(
            {"version": "1.2.3-1.fc37"},
            (
                "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/Packages/h/hoppr-test-rpm-1.2.3-1.fc37.x86_64.rpm",
                "sha256",
                "ae9ade07f27932aa94a4f5c407338848c3488393cbe678ec6baa2fe86514171a",
            ),
            None,
            id="hoppr-test-rpm@1.2.3",
        ),
        pytest.param(
            {"version": "4.5.6-7.fc37"},
            (
                "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/Packages/h/hoppr-test-rpm-4.5.6-7.fc37.x86_64.rpm",
                "sha256",
                "80533ab0c74f574156ced118e6c429ba045c1697646fdee441533618e90a41f3",
            ),
            None,
            id="hoppr-test-rpm@4.5.6",
        ),
        pytest.param(
            {"version": "1.2.3"},
            None,
            "Failed to parse version string from PURL: "
            "'pkg:rpm/fedora/hoppr-test-rpm@1.2.3?arch=x86_64&distro=fedora-37&epoch=0'",
            id="version-no-dash",
        ),
        pytest.param(
            {"name": "missing-test-rpm"},
            None,
            "RPM package not found in repository: "
            "'pkg:rpm/fedora/missing-test-rpm@1.2.3-1.fc37?arch=x86_64&distro=fedora-37&epoch=0'",
            id="package-not-found",
        ),
        pytest.param(
            {
                "qualifiers": {
                    "arch": "x86_64",
                    "distro": "fedora-37",
                    "epoch": "0",
                    "repository_url": "https://rpm.other.com/dl/8/AppStream/x86_64/os",
                }
            },
            None,
            "Purl-specified repository url (https://rpm.other.com/dl/8/AppStream/x86_64/os) "
            "does not match current repo (https://rpm.hoppr.com/dl/8/AppStream/x86_64/os).",
            id="download-url-not-in-manifest",
        ),
    ],
    indirect=["pkg_url"],
)
def test__get_download_url(
    pkg_url: PackageURL,
    expected_download_info: tuple[str, str, str] | None,
    expected_exception: str | None,
    plugin_fixture: CollectRpmPlugin,
    monkeypatch: MonkeyPatch,
):
    """Test CollectRpmPlugin._get_download_url method."""
    repo = "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"

    monkeypatch.setitem(
        dic=plugin_fixture.rpm_data,
        name=repo,
        value=xmltodict.parse(xml_input=TEST_PRIMARY_XML, force_list=["package"]),
    )

    download_info: tuple[str, str, str] | None = None

    try:
        download_info = plugin_fixture._get_download_url(purl=pkg_url, repo_url=repo)
    except HopprPluginError as ex:
        assert str(ex) == expected_exception

    assert download_info == expected_download_info


@pytest.mark.parametrize(
    argnames=["expected_download_info", "pkg_url", "response_generator"],
    argvalues=[
        pytest.param(
            (
                "https://rpm.hoppr.com/dl/8/BaseOS/x86_64/os/Packages/h/hoppr-test-rpm-1.2.3-1.fc37.x86_64.rpm",
                "sha256",
                "ae9ade07f27932aa94a4f5c407338848c3488393cbe678ec6baa2fe86514171a",
            ),
            {
                "qualifiers": {
                    "arch": "x86_64",
                    "distro": "fedora-37",
                    "epoch": "0",
                    "repository_url": "https://rpm.hoppr.com/dl/8/BaseOS/x86_64/os",
                }
            },
            [
                {"content": TEST_REPOMD_XML.encode(encoding="utf-8")},
                {"content": b"0" * 1024},
                {"content": b"0" * 1024},
                {"content": b"0" * 1024},
                {"content": gzip.compress(data=TEST_PRIMARY_XML.encode(encoding="utf-8"))},
            ],
            id="repository_url-qualifier-no-strict",
        ),
    ],
    indirect=["pkg_url", "response_generator"],
)
def test__get_download_url_no_strict(
    pkg_url: PackageURL,
    expected_download_info: tuple[str, str, str] | None,
    response_generator: Callable[..., requests.Response],
    plugin_fixture: CollectRpmPlugin,
    monkeypatch: MonkeyPatch,
):
    """Test CollectRpmPlugin._get_download_url method with `--no-strict` option."""
    repo = "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"

    monkeypatch.setattr(target=requests, name="get", value=response_generator)
    monkeypatch.setattr(target=plugin_fixture.context, name="strict_repos", value=False)
    monkeypatch.setitem(
        dic=plugin_fixture.rpm_data,
        name=repo,
        value=xmltodict.parse(xml_input=TEST_PRIMARY_XML, force_list=["package"]),
    )

    download_info = plugin_fixture._get_download_url(purl=pkg_url, repo_url=repo)

    assert download_info == expected_download_info


@pytest.mark.parametrize(
    argnames=["expected_exception", "response_fixture"],
    argvalues=[
        pytest.param(
            None,
            {"content": gzip.compress(TEST_PRIMARY_XML.encode(encoding="utf-8"))},
            id="primary-xml-success",
        ),
        pytest.param(
            "Failed to get primary XML data from "
            "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/repodata/test-primary.xml.gz",
            {"status_code": 404},
            id="primary-xml-fail",
        ),
    ],
    indirect=["response_fixture"],
)
def test__get_primary_xml_data(
    expected_exception: str | None,
    plugin_fixture: CollectRpmPlugin,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: MonkeyPatch,
):
    """Test CollectRpmPlugin._get_primary_xml_data method."""
    repo_url = RepositoryUrl(url="https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/")
    repomd_dict = xmltodict.parse(xml_input=TEST_REPOMD_XML, force_list=["data"])

    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    try:
        rpm_data = plugin_fixture._get_primary_xml_data(repo_url, repomd_dict)
        expected_data = xmltodict.parse(xml_input=TEST_PRIMARY_XML, force_list=["package"])
        assert rpm_data == expected_data
    except HopprPluginError as ex:
        assert str(ex) == expected_exception


@pytest.mark.parametrize(
    argnames=["expected_exception", "response_fixture"],
    argvalues=[
        pytest.param(
            None,
            {"content": TEST_REPOMD_XML.encode(encoding="utf-8")},
            id="repodata-success",
        ),
        pytest.param(
            "Failed to get repository metadata from https://rpm.hoppr.com/dl/8/AppStream/x86_64/os",
            {"status_code": 404},
            id="repodata-fail",
        ),
    ],
    indirect=["response_fixture"],
)
def test__get_repodata(
    expected_exception: str | None,
    plugin_fixture: CollectRpmPlugin,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: MonkeyPatch,
):
    """Test CollectRpmPlugin._get_repodata method."""
    repo_url = RepositoryUrl(url="https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/")

    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    try:
        repodata = plugin_fixture._get_repodata(repo_url)
        expected_data = xmltodict.parse(xml_input=TEST_REPOMD_XML, force_list=["data"])
        assert repodata == expected_data
    except HopprPluginError as ex:
        assert str(ex) == expected_exception


@pytest.mark.parametrize(
    argnames=["expected_exception", "cred_object_fixture", "response_generator"],
    argvalues=[
        pytest.param(
            None,
            {"url": "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"},
            [
                {"content": TEST_REPOMD_XML.encode(encoding="utf-8")},
                {"content": b"0" * 1024},
                {"content": b"0" * 1024},
                {"content": b"0" * 1024},
                {"content": gzip.compress(data=TEST_PRIMARY_XML.encode(encoding="utf-8"))},
            ],
            id="rpm-data-success",
        ),
        pytest.param(
            "Failed to get repository metadata from https://rpm.hoppr.com/dl/8/AppStream/x86_64/os",
            {"url": "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"},
            [{"status_code": 404}, {"status_code": 404}, {"status_code": 404}],
            id="rpm-data-fail",
        ),
    ],
    indirect=["cred_object_fixture", "response_generator"],
)
def test__populate_rpm_data(
    expected_exception: str | None,
    find_credentials_fixture: Callable[[str], CredentialRequiredService],
    plugin_fixture: CollectRpmPlugin,
    response_generator: Callable[..., requests.Response],
    monkeypatch: MonkeyPatch,
):
    """Test CollectRpmPlugin._populate_rpm_data method."""
    repo_url = RepositoryUrl(url="https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/")

    with monkeypatch.context() as patch:
        patch.setattr(target=CollectRpmPlugin, name="rpm_data", value={})
        patch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
        patch.setattr(target=requests, name="get", value=response_generator)

        try:
            plugin_fixture._populate_rpm_data(repo_url)
            assert CollectRpmPlugin.rpm_data[str(repo_url)] == xmltodict.parse(
                xml_input=TEST_PRIMARY_XML, force_list=["package"]
            )

            # Run a second time to assert idempotence
            plugin_fixture._populate_rpm_data(repo_url)
            assert len(CollectRpmPlugin.rpm_data) == 1
        except HopprPluginError as ex:
            assert str(ex) == expected_exception


@pytest.mark.parametrize(
    argnames=["expected_exception", "response_fixture"],
    argvalues=[
        pytest.param(
            None,
            {"content": TEST_REPOMD_XML.encode(encoding="utf-8")},
            id="stream-success",
        ),
        pytest.param(
            "Failed to retrieve data from https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/repodata/repomd.xml",
            {"status_code": 404},
            id="stream-fail",
        ),
    ],
    indirect=["response_fixture"],
)
def test__stream_url_data(
    expected_exception: str | None,
    plugin_fixture: CollectRpmPlugin,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: MonkeyPatch,
):
    """Test CollectRpmPlugin._stream_url_data method."""
    repomd_url = "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os/repodata/repomd.xml"

    monkeypatch.setattr(target=requests, name="get", value=response_fixture)

    try:
        response = plugin_fixture._stream_url_data(url=RepositoryUrl(url=repomd_url))
        assert response.url == repomd_url
    except requests.HTTPError as ex:
        assert str(ex) == expected_exception


@pytest.mark.parametrize(
    argnames=["pkg_url", "response_fixture", "expected_result"],
    argvalues=[
        pytest.param(
            {"version": "7.8.9-1.fc37"},
            {},
            Result.fail(
                message=(
                    "Failure after 3 attempts, final message RPM package not found in repository: "
                    "'pkg:rpm/fedora/hoppr-test-rpm@7.8.9-1.fc37?arch=x86_64&distro=fedora-37&epoch=0'"
                )
            ),
            id="rpm-data-not-found",
        ),
        pytest.param(
            {},
            {"content": b"download fail", "status_code": 404},
            Result.fail(message="HTTP Status Code: 404; download fail"),
            id="rpm-download-fail",
        ),
        pytest.param(
            {},
            {"content": b"0" * 1048576},
            Result.success(
                message="",
                return_obj=Component(
                    name="hoppr-test-rpm",
                    version="1.2.3-1.fc37",
                    purl="pkg:rpm/fedora/hoppr-test-rpm@1.2.3-1.fc37?arch=x86_64&distro=fedora-37&epoch=0",
                    type=ComponentType.LIBRARY,
                ),
            ),
            id="collect-success",
        ),
    ],
    indirect=["pkg_url", "response_fixture"],
)
def test_collect(
    expected_result: Result,
    component: Component,
    plugin_fixture: CollectRpmPlugin,
    response_fixture: Callable[[str], requests.Response],
    monkeypatch: MonkeyPatch,
):
    """Test CollectRpmPlugin.collect method."""
    repo_url = "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"

    monkeypatch.setattr(target=requests, name="get", value=response_fixture)
    monkeypatch.setattr(
        target=hoppr.net,
        name="get_file_hash",
        value=lambda *_, **__: "ae9ade07f27932aa94a4f5c407338848c3488393cbe678ec6baa2fe86514171a",
    )
    monkeypatch.setitem(
        dic=plugin_fixture.rpm_data,
        name=repo_url,
        value=xmltodict.parse(xml_input=TEST_PRIMARY_XML, force_list=["package"]),
    )

    result = plugin_fixture.collect(component, repo_url)

    assert result.status == expected_result.status
    assert result.message == expected_result.message
    assert result.return_obj == expected_result.return_obj


def test_pre_stage_process(plugin_fixture: CollectRpmPlugin, sbom: Sbom, monkeypatch: MonkeyPatch):
    """Test CollectRpmPlugin.pre_stage_process method."""
    rpm_data = xmltodict.parse(xml_input=TEST_PRIMARY_XML, force_list=["package"])

    def _populate_rpm_data_patch(repo_url: RepositoryUrl):
        assert str(repo_url) in {
            "https://rpm.hoppr.com/dl/8/AppStream/x86_64/os",
            "https://rpm.hoppr.com/dl/8/BaseOS/x86_64/os",
        }

        CollectRpmPlugin.rpm_data[str(repo_url)] = rpm_data

    monkeypatch.setattr(target=plugin_fixture.context, name="consolidated_sbom", value=sbom)
    monkeypatch.setattr(target=plugin_fixture, name="_populate_rpm_data", value=_populate_rpm_data_patch)

    result = plugin_fixture.pre_stage_process()

    assert result.is_success()
    assert plugin_fixture.rpm_data["https://rpm.hoppr.com/dl/8/AppStream/x86_64/os"] == rpm_data
    assert plugin_fixture.rpm_data["https://rpm.hoppr.com/dl/8/BaseOS/x86_64/os"] == rpm_data
